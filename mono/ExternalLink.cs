using System;
using System.Collections.Generic;

public class ExternalLink : IToken
{
    public TokenType Type  { get; set; }
    public string Text { get; set; }
    public string Value { get; set; }
    public string Url { get; set; }

    public ExternalLink (Page page, Tokens tokens)
    {
        var contents = new Tokens();
        while (! tokens.Empty ())
        {
            var token = tokens.Dequeue ();
            switch (token.Type)
            {
                case TokenType.CloseExternalLink:
                    init (contents, token);
                    return;
                default:
                    contents.Add (token);
                    break;
            }
        }
        tokens.InsertRange (0, contents);
        throw new ParseException ("Unclosed external link on " + page.Title + ": " + contents.Preview ());
    }

    private void init (Tokens tokens, IToken token)
    {
	string s = "http" + tokens.Text;
        Text = "[" + s + "]";
        Type = TokenType.ExternalLink;
        // Console.WriteLine ("Found external link: " + Text);
        var firstSpaceIndex = s.IndexOf(" ");
        if (firstSpaceIndex > 0)
        {
            Url = s.Substring(0, firstSpaceIndex);
            Value = s.Substring(firstSpaceIndex + 1); 
        }
        else
	{
	   Url = s;
           Value = "";
        } 
        // Console.WriteLine ("Url=" + Url + " Value=" + Value);
    }

    public override string ToString ()
    {
        return Text;
    }
}

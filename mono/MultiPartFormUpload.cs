using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;

public class CookieAwareWebClient : WebClient
{
    private readonly CookieContainer cookieContainer = new CookieContainer();

    protected override WebRequest GetWebRequest(Uri address)
    {
        WebRequest request = base.GetWebRequest(address);
        HttpWebRequest webRequest = request as HttpWebRequest;
        if (webRequest != null)
        {
            webRequest.CookieContainer = cookieContainer;
        }
        return request;
    }

    public class MimePart
    {
        private NameValueCollection headers = new NameValueCollection();
        private byte[] header;

        public NameValueCollection Headers
        {
            get { return headers; }
        }

        public byte[] Header
        {
            get { return header; }
        }

        public long GenerateHeaderFooterData(string boundary)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("--");
            stringBuilder.Append(boundary);
            stringBuilder.AppendLine();
            foreach (string key in headers.AllKeys)
            {
                stringBuilder.Append(key);
                stringBuilder.Append(": ");
                stringBuilder.AppendLine(headers[key]);
            }
            stringBuilder.AppendLine();

            header = Encoding.UTF8.GetBytes(stringBuilder.ToString());

            return header.Length + Data.Length + 2;
        }

        public string Data { get; set; }
    }

    public class UploadResponse
    {
        public HttpStatusCode HttpStatusCode { get; set; }
        public string ResponseBody { get; set; }

        public UploadResponse(HttpStatusCode httpStatusCode, string responseBody)
        {
            HttpStatusCode = httpStatusCode;
            ResponseBody = responseBody;
        }
    }

    public UploadResponse Upload (string url, NameValueCollection requestHeaders, NameValueCollection requestParameters, NameValueCollection multiparts)
    {
        try
        {
            List<MimePart> mimeParts = new List<MimePart>();

            foreach (string key in requestHeaders.AllKeys)
            {
                Headers.Add(key, requestHeaders[key]);
            }

            foreach (string key in requestParameters.AllKeys)
            {
                MimePart part = new MimePart();
                part.Headers["Content-Disposition"] = "form-data; name=\"" + key + "\"";
                part.Data = requestParameters[key];
                mimeParts.Add(part);
            }

            foreach (string key in multiparts.AllKeys)
            {
                MimePart part = new MimePart();
                part.Headers["Content-Disposition"] = "form-data; name=\"" + key + "\"";
                part.Headers["Content-Type"] = "text/html; charset=UTF-8";
                part.Data = multiparts[key];
                mimeParts.Add(part);
            }

            string boundary = "----------" + DateTime.Now.Ticks.ToString("x");
            Headers.Add(HttpRequestHeader.ContentType, "multipart/form-data; boundary=" + boundary);

            long contentLength = 0;

            byte[] footer = Encoding.UTF8.GetBytes("--" + boundary + "--\r\n");

            foreach (MimePart mimePart in mimeParts)
            {
                contentLength += mimePart.GenerateHeaderFooterData(boundary);
            }

            using (MemoryStream memoryStream = new MemoryStream())
            {
                foreach (MimePart mimePart in mimeParts)
                {
                    memoryStream.Write (mimePart.Header, 0, mimePart.Header.Length);
                    byte[] buffer = Encoding.UTF8.GetBytes(mimePart.Data + "\r\n");
                    memoryStream.Write (buffer, 0, buffer.Length);
                }

                memoryStream.Write(footer, 0, footer.Length);

//              Console.WriteLine (System.Text.Encoding.UTF8.GetString (memoryStream.ToArray()));

                byte[] responseBytes = UploadData(url, memoryStream.ToArray());
                string responseString = Encoding.UTF8.GetString(responseBytes);
                return new UploadResponse(HttpStatusCode.OK, responseString);
            }
        }
        catch (WebException ex)
        {
            WebException webException = (WebException)ex;
            using (HttpWebResponse response = (HttpWebResponse)webException.Response)
            using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
            {
                string responseString = reader.ReadToEnd();
                return new UploadResponse(response.StatusCode, responseString);
            }
        }
        finally
        {
            GC.Collect();    
        }
   }
}

using System;
using System.Collections.Generic;

public class Nowiki : IToken
{
    public TokenType Type  { get; set; }
    public string Text { get; set; }
    public string Value { get; set; }

    public Nowiki (Page page, Tokens tokens)
    {
        var contents = new Tokens();
        while (! tokens.Empty ())
        {
            var token = tokens.Dequeue ();
            switch (token.Type)
            {
                case TokenType.CloseNowiki:
                    init (contents, token);
                    return;
                default:
                    contents.Add (token);
                    break;
            }
        }
        tokens.InsertRange (0, contents);
        throw new ParseException ("Unclosed <nowiki> on " + page.Title + ": " + contents.Preview ());
    }

    private void init (Tokens tokens, IToken token)
    {
        Value = tokens.Text;
        Text = "<nowiki>" + Value + token;
        Type = TokenType.Nowiki;
    }

    public override string ToString ()
    {
        return Text;
    }
}

using System;

public struct Revision
{
    public int RevId        { get; set; }    
    public int ParentId     { get; set; }
    public string Title     { get; set; }
    public string Comment   { get; set; }
    public DateTime Timestamp { get; set; }
    public string User      { get; set; }      
}

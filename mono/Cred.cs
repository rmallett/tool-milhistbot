using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Xml;

#pragma warning disable 618

public class CredException : Exception
{
    public Cred Cred {get; set; }

    public CredException(Cred cred, string message) : base(message)
    {
        Cred = cred;
    }
}

public class Cred {
    public string Job { get; set; }
    public string User { get; set; }
    public string Password { get; set; }
    public string LogsDirectory { get; set; }

    private StreamWriter log;
    private string mailDelivery;
    private List<string> mailList;
    private string smtpHost;

    public void mail (string from, string to, string message) {
        MailMessage mail = new MailMessage(from + "@anu.edu.au", to);
        mail.Subject = "Error report";
        mail.Body = message;
        SmtpClient client = new SmtpClient();
        client.Host = smtpHost;
        client.Send(mail);
    }

    public string timestamp () {
        return DateTime.Now.ToString("HH:mm dd MMMM yyyy");
    }

    public void Warning (string message) {
        string t = timestamp () + " " + message;
        Console.Error.WriteLine (t);
        var log = Log ();
        log.WriteLine (t);
        log.Flush ();
        foreach (string to in mailList)
        {
            mail (User, to, message);
        }
    }

    public void Showtime (string message) {
        string t = timestamp () + " " + message;
        Console.WriteLine (t);
        var log = Log ();
        log.WriteLine (t);
        log.Flush ();
    }

    public StreamWriter Log ()
    {
        if (log == null)
        {
            string logFileName = Path.Combine (LogsDirectory, Job + ".log");
            log = new StreamWriter (logFileName, true, Encoding.UTF8);
        }
        return log;
    }

    public void Close ()
    {
        if (log != null)
        {
            log.Close ();
            log = null;
        }
    }

    public Cred() {
        // Get the credentials file
        LogsDirectory   = "/tmp";
        string fullname = Environment.GetCommandLineArgs()[0];
        Job             = Path.GetFileNameWithoutExtension(fullname);
        string pathname = Path.GetDirectoryName(fullname);
        string credx    = Path.Combine (pathname, "credx.xml");

        // Find the job + get the user
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.Load(credx);
        XmlNode rootNode = xmlDocument.DocumentElement;
        XmlNode userNode = rootNode.SelectSingleNode("descendant::job[name='" + Job + "']");
        if (null == userNode)
        {
            throw new CredException (this, "job " + Job + " not found in " + credx);
        }
        User = userNode["user"].InnerText;

        //  Now we have the user, extract the other details
        XmlNode credNode = rootNode.SelectSingleNode("descendant::cred[user='" + User + "']");
        Password = credNode["password"].InnerText;

        XmlNodeList mailNodeList = credNode.SelectNodes("descendant::mail");
        mailList = new List<string>();
        foreach (XmlNode mailNode in mailNodeList) {
            mailDelivery = mailNode["delivery"].InnerText;
            mailList.Add (mailNode["to"].InnerText);
        }

        XmlNode logsNode = rootNode.SelectSingleNode("descendant::logs");
        LogsDirectory = logsNode["directory"].InnerText;

        XmlNode smtpHostNode = rootNode.SelectSingleNode("descendant::smtphost");
        smtpHost = smtpHostNode.InnerText;
    }
}

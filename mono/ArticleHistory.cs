using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

public interface IMilestone
{
	string Action { get; }
    string Date   { get; }
    string Link   { get; }
    int RevId     { get; }
	string Result { get; }
}

public class ArticleForDeletion : Template, IMilestone
{
    private DateTime _timestamp;
    private bool _timestampSet = false;

	public static bool IsArticleForDeletion (Template template)
	{
	    return template.Name.Equals ("old AfD", StringComparison.OrdinalIgnoreCase) ||  template.Name.Equals ("Old Afd multi", StringComparison.OrdinalIgnoreCase);
	}

	public string Action
	{
		get => "AFD";
	}

    public string Date
    {
		get => timestamp ().ToString ("HH:mm:ss dd MMMM yyyy (UTC)");
	}

    public string Link
    {
        get => "Wikipedia:Articles for deletion/" + (Exists ("page") ? Find ("page").Value : Page.Article.Title);
    }

	public string Result
	{
		get => Find ("result").Value.Trim (new Char []{'\''});
	}

    public int RevId
    {
        get => ArticleHistory.GetRevId (Page.Article, Date);
	}

	private DateTime timestamp ()
	{
		if (! _timestampSet)
		{
		    _timestamp = Exists ("date") ? ArticleHistory.ParseTimestamp (Find ("date").Value) : Page.Article.History (1).First ().Timestamp;
		    _timestampSet = true;
		}
		return _timestamp;
	}

    public ArticleForDeletion (Template template) : base (template)
    {
        Page.Templates.Add (this);
    }

}

public class GoodArticle : Template, IMilestone
{
    private string _timestamp;

	public static bool IsGoodArticle (Template template)
	{
	    return template.Name.Equals ("GA", StringComparison.OrdinalIgnoreCase);
	}

	public string Action
	{
		get => "GA";
	}

    public string Date
    {
		get => ArticleHistory.FormatTimestamp (timestamp ());
	}

    public string Link
    {
        get => Page.Talk.Title + "/GA" + (Exists ("page") ? Find ("page").Value : "1");
    }

	public string Result
	{
		get => "listed";
	}

    public int RevId
    {
        get => Exists ("oldid") ? Convert.ToInt32 (Find ("oldid").Value) : ArticleHistory.GetRevId (Page.Article, timestamp ());
	}

	public string timestamp ()
	{
		if (null == _timestamp)
		{
			_timestamp = Exists ("date") ? Find ("date").Value : Find ("1").Value;
		}
		return _timestamp;
	}

    public GoodArticle (Template template) : base (template)
    {
        Page.Templates.Add (this);
    }

}

public class OldPeerReview : Template, IMilestone
{
    private DateTime _timestamp;
    private bool _timestampSet = false;

	public static bool IsOldPeerReview (Template template)
	{
	    return template.Name.Equals ("Old peer review", StringComparison.OrdinalIgnoreCase) || template.Name.Equals ("Oldpeerreview", StringComparison.OrdinalIgnoreCase);
	}

    public string name ()
    {
        return Exists ("1") ? Find ("1").Value : Exists ("reviewedname") ? Find ("reviewedname").Value : Page.Article.Title;
    }

	public DateTime timestamp ()
	{
	    if (! _timestampSet)
	    {
    		var linkPage = new Page (Page.Bot, Link);
    		var revision = linkPage.History (1).First ();
     		_timestamp = revision.Timestamp;
    		_timestampSet = true;
     	}
		return _timestamp;
	}

	public string Action
	{
		get => "PR";
	}

    public string Date
    {
		get
		{
		    if (Exists ("date"))
		    {
		        try
		        {
    		        var date = ArticleHistory.FormatTimestamp (Find ("date").Value);
    		        if (! date.Matches ("00:00:00 01") )
    		        {
    		            return date;
    		        }
    		    }
    		    catch (ArticleHistoryDateException)
    		    {
    		        // Drop through
    		    }
		    }
            return timestamp ().ToString ("HH:mm:ss dd MMMM yyyy (UTC)");
		}
	}

    public string Link
    {
    	get
    	{
	    	string link;
	        if (Exists ("archivelink"))
	        {
	            link = Find ("archivelink").Value;
	        }
	        else
	        {
	            string archive = Exists ("archive") ? "/archive" + Find ("archive").Value : "";
	            link = "Wikipedia:Peer review/" + name () + archive;
	        }
	        return link;
	    }
    }

	public string Result
	{
		get => "reviewed";
	}

    public int RevId
    {
    	get => Exists ("ID") ? Int32.Parse (Find ("ID").Value) : ArticleHistory.GetRevId (Page.Article, timestamp ());
	}

    public OldPeerReview (Template template) : base (template)
    {
        Page.Templates.Add (this);
    }
}

public class Milestone: IMilestone
{
	private int id;
	private string action;
	private string date;
	private string link;
	private int revId;
	private string result;

	public string Action
	{
		get => action;
	}

    public string Date
    {
		get => date;
	}

    public string Link
    {
        get => link;
    }

    public int RevId
    {
        get => revId;
	}

	public string Result
	{
		get => result;
	}

	public override string ToString ()
	{
        return ("id=" + id + " date=" + Date  + " link=" + Link + " oldid=" + RevId  + " result=" + Result);
	}

	public Milestone (int id, string action, string date, string link, int revId, string result)
	{
		this.id     = id;
		this.action = action;
		this.date   = date;
		this.link   = link;
		this.revId  = revId;
		this.result = result;
	}

	public Milestone (string action, string date, string link, int revId, string result) : this (-1, action, date, link, revId, result)
	{
	}

}

public interface IArticleHistoryItem
{
	Dictionary<string, string> KeyValuePairs ();
}

public class DidYouKnow : Template, IArticleHistoryItem
{
	Dictionary<string, string> dyk;

	public static bool IsDidYouKnow (Template template)
	{
         return template.Name.Equals ("DYK talk", StringComparison.OrdinalIgnoreCase)  ||  template.Name.Equals ("DYKtalk", StringComparison.OrdinalIgnoreCase);
	}

	public Dictionary<string, string> KeyValuePairs ()
	{
		if (null == dyk)
		{
			dyk = new Dictionary<string, string>();
	        string date    = Find ("1").Value + " " + Find ("2").Value;
	        string entry   = Exists ("entry")   ? Find ("entry").Value   : Exists ("3") ? Find ("3").Value : "";
	        string nompage = Exists ("nompage") ? Find ("nompage").Value : "Template:Did you know nominations/" + Page.Article.Title;
	        dyk.Add ("dykdate",  date);
	        dyk.Add ("dykentry", entry);
	        dyk.Add ("dyknom",   nompage);
	    }
	    return dyk;
    }

    public DidYouKnow (Template template) : base (template)
    {
        Page.Templates.Add (this);
    }
}

public class InTheNews : Template, IArticleHistoryItem
{
	Dictionary<string, string> itn;

	public static bool IsInTheNews (Template template)
	{
         return template.Name.Equals ("ITN talk", StringComparison.OrdinalIgnoreCase) ||  template.Name.Equals ("ITNtalk", StringComparison.OrdinalIgnoreCase);
	}

	public Dictionary<string, string> KeyValuePairs ()
	{
		if (null == itn)
	    {
			itn = new Dictionary<string, string>();
	   	    if (Exists ("1") && Exists ("2"))
	   	    {
	   	        string timestamp = Find ("1").Value + " " + Find ("2").Value;
	        	string date      = ArticleHistory.FormatTimestamp (timestamp);
	   	        itn.Add ("itndate", date);
	   	    }
	   	    if (Exists ("date"))
	   	    {
	   	        string timestamp = Find ("date").Value;
	        	string date      = ArticleHistory.FormatTimestamp (timestamp);
	   	        itn.Add ("itndate", date);
	   	    }
	   	    for (int i = 1; ; ++i)
	   	    {
	   	        if (Exists ("date" + i))
	   	        {
	   	            string timestamp = Find ("date" + i).Value;
	        		string date      = ArticleHistory.FormatTimestamp (timestamp);
	   	            itn.Add ("itn" + i + "date", date);
	   	            continue;
	   	        }
	   	        break;
	   	    }
	    }
	    return itn;
	}

    public InTheNews (Template template) : base (template)
    {
        Page.Templates.Add (this);
    }
}

public class OnThisDay : Template, IArticleHistoryItem
{
	Dictionary<string, string> otd;

	static public bool IsOnThisDay (Template template)
	{
         return  template.Name.Equals ("OTD talk", StringComparison.OrdinalIgnoreCase)    ||  template.Name.Equals ("OTDtalk", StringComparison.OrdinalIgnoreCase) ||
                 template.Name.Equals ("On this day", StringComparison.OrdinalIgnoreCase) ||  template.Name.Equals ("Onthisday", StringComparison.OrdinalIgnoreCase);
	}

	public Dictionary<string, string> KeyValuePairs ()
	{
		if (null == otd)
		{
    		Page article = Page.Article;
			otd = new Dictionary<string, string>();
    	  	for (int i = 1; ; ++i)
        	{
            	if (Exists ("date" + i))
            	{
            		string date = ArticleHistory.FormatTimestamp (Find ("date" + i). Value);
                	otd.Add ("otd" + i + "date", date);
                	if (Exists ("oldid" + i))
                	{
                    	string oldid = Find ("oldid" + i).Value;
                    	otd.Add ("otd" + i + "date", oldid + "\n");
                	}
                	else
                	{
                    	int revid = ArticleHistory.GetRevId (article, date);
                    	otd.Add ("otd" + i + "date", revid + "\n");
                	}
                	continue;
            	}
            	break;
            }
        }
        return otd;
    }

    public OnThisDay (Template template) : base (template)
    {
        Page.Templates.Add (this);
    }
}

public class ArticleHistoryException : Exception
{
    public ArticleHistoryException(string message) : base (message)
    {
    }
}

public class ArticleHistoryDateException : ArticleHistoryException
{
    public ArticleHistoryDateException(string message) : base (message)
    {
    }
}

public class ArticleHistory : Template,  IComparer<Milestone>
{

	public static bool IsArticleHistory (Template template)
	{
	    return Regex.IsMatch (template.Name, "Article *History", RegexOptions.IgnoreCase);
	}

    public Parameter FindAction (string action, string value)
    {
        var last = FindLast (@"action\d+", action);
        if (null != last)
        {
            return Find (last.Name + value);
        }
        return null;
    }

	public static string FormatTimestamp (string timestamp)
	{
		return ParseTimestamp (timestamp).ToString ("HH:mm:ss dd MMMM yyyy (UTC)");
	}

    public static DateTime ParseTimestamp (string timestamp)
    {
        DateTime result;
        bool parsed = false;

        if (DateTime.TryParse (timestamp, out result))
        {
        	parsed = true;
       	}
       	else
       	{
		    string[] patterns = {
		        "HH:mm, d MMMM yyyy (UTC)",
		        "HH:mm:ss d MMMM yyyy (UTC)"
		    };

		    foreach (var pattern in patterns)
		    {
		        if (DateTime.TryParseExact (timestamp, pattern, null, DateTimeStyles.None, out result))
		        {
		            // Debug.WriteLine("Parsed '{0}' as {1}.",  timestamp, result.ToString("HH:mm:ss dd MMMM yyyy (UTC)"));
		            parsed = true;
		            break;
		        }
		    }
		}

		if (! parsed)
		{
			throw new ArticleHistoryDateException ("unable to parse timestamp '" + timestamp + "'");
		}

		return result;
    }

	public static int GetRevId (Page page, DateTime dt)
    {
        // Debug.Entered ("dt=" + dt.ToString("HH:mm:ss dd MMMM yyyy (UTC)"));
        do
        {
            var revisions = page.History (500);
            foreach (var revision in revisions)
            {
                DateTime dr = revision.Timestamp;
                // Debug.WriteLine ("dr=" + dr.ToString("HH:mm:ss dd MMMM yyyy (UTC)"));
                if (DateTime.Compare (dr, dt) <= 0)
                {
                    // Debug.Exited ("revid=" + revision.RevId);
					return revision.RevId;
                }
            }
        } while (page.Query.Continue);
        throw new ArticleHistoryException ("unable to find revision for " + page.Title);
    }

	public static int GetRevId (Page page, string time)
    {
        return GetRevId (page, ParseTimestamp (time));
    }

    private int nextActionId ()
    {
        int next = 1;
        var last = FindLast (@"action\d+");
        if (null != last)
        {
            MatchCollection matches = Regex.Matches (last.Name, @"action(\d+)");
            foreach (Match match in matches)
            {
                next = Int32.Parse(match.Groups[1].Value) + 1;
            }
        }
        return next;
    }

	public void Add (int id, IMilestone action)
	{
        Add ("action" + id,            action.Action + "\n");
        Add ("action" + id + "date",   action.Date   + "\n");
        Add ("action" + id + "link",   action.Link   + "\n");
        Add ("action" + id + "result", action.Result + "\n");
        Add ("action" + id + "oldid",  action.RevId  + "\n");
	}

	public void Add (IMilestone action)
	{
	    if (! Exists (action))
	    {
            Add (nextActionId (), action);
        }
	}

	public void Add (IArticleHistoryItem articleHistoryItem)
	{
		var keyValuePairs = articleHistoryItem.KeyValuePairs ();
		foreach (var pair in keyValuePairs)
		{
			Add (pair.Key, pair.Value + "\n");
		}
	}

	public void Add (Template template)
	{
        if (template is IMilestone)
        {
            Add (template as IMilestone);
        }
        if (template is IArticleHistoryItem)
        {
            Add (template as IArticleHistoryItem);
		}
        if (template is GoodArticle)
        {
        	Add ("topic", template.Find ("topic").Value + "\n");
        }
    }

    public void Collapse (string value)
    {
    	Add ("collapse", value);
    }

	public int Compare (Milestone x, Milestone y)
	{
		DateTime dx = ArticleHistory.ParseTimestamp (x.Date);
		DateTime dy = ArticleHistory.ParseTimestamp (y.Date);
		return DateTime.Compare (dx, dy);
	}

    public bool Exists (IMilestone action)
    {
        bool exists = false;
        var links = FindAll  (@"action\d+link");
        foreach (var link in links)
        {
            if (link.Value.Equals (action.Link))
            {
                Debug.WriteLine ("found duplicate action: " + action);
                exists = true;
            }
        }
        return exists;
    }

	public Milestone Get (int id)
	{
	    Debug.Entered ("id = " + id);
		string action = Find   ("action" + id).Value;
		string date   = Find   ("action" + id + "date").Value;
        string link   = Exists ("action" + id + "link") ? Find ("action" + id + "link").Value : "";
        string result = Find   ("action" + id + "result").Value;
        int oldid     = Exists ("action" + id + "oldid") && ! Find ("action" + id + "oldid").Value.Equals ("") ? int.Parse (Find ("action" + id + "oldid").Value) : GetRevId (Page.Article, date);
        Milestone milestone = new Milestone (id, action, date, link, oldid, result);
        Debug.Exited ("milestone = " + milestone.ToString ());
		return milestone;
	}

	public Milestone GetLast (string action)
	{
	    Debug.Entered ("action = " + action);
		Parameter actionParameter = FindLast   (@"action\d+", action);
		if (null == actionParameter)
		{
			return null;
		}
		string idString = Regex.Match(actionParameter.Name, @"\d+").Value;
		int id = Int32.Parse (idString);
        Milestone milestone = Get (id);
        Debug.Exited ("milestone = " + milestone.ToString ());
		return milestone;
	}

    public void Merge ()
    {
    	var templates = Page.Templates.FindAll (ind=>ind is IMilestone || ind is IArticleHistoryItem);
    	foreach (var template in templates)
    	{
            Add (template);
	        Page.Remove (template);
	    }
    }

	private void moveToEnd (string name)
	{
		var parameter = Find (name);
		if (null != parameter)
		{
    		Remove (parameter);
    		Add (parameter);
        }
	}

	public void Sort ()
	{
		List<Milestone> milestones = new List<Milestone>();

		List<Parameter> actionIdParameters = FindAll (@"^action\d+$");
		foreach (var p in actionIdParameters)
		{
			int id = int.Parse (p.Name.Substring (6));
			milestones.Add (Get (id));
		}

		milestones.Sort (Compare);

		// Covering the odd case where the originals were not consecutive
		{
		    List<Parameter> actionParameters = FindAll (@"^action\d");
			foreach (var parameter in actionParameters)
			{
				Remove (parameter);
			}
		}

		{
			int id = 1;
			foreach (var milestone in milestones)
			{
				Add (id++, milestone);
			}
		}

		moveToEnd ("currentstatus");
		moveToEnd ("topic");
	}

    public ArticleHistory (Template template): base (template)
    {
        Page.Templates.Add (this);
    }
}

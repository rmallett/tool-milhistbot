using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class MilHist
{
    private const string milHistTemplateNameRegex = "WikiProject Military History|WPCAS|WikiProject Colditz|MILHIST|Milhist|WP Military History|WikiProject MILHIST|WikiProject War|WP Military history|WPMILHIST|WPMilhist|MilHist|Mil Hist|WPMH|WP Mil|Military history|WPMIL|WP MILHIST|WikiProject Military|Wpmil";
    private Page page;
    private MilHistProjectTemplate projectTemplate;
    private Coordinators coordinators;

    public MilHistProjectTemplate ProjectTemplate
    {
        get
        {
            if (null == projectTemplate)
            {
                projectTemplate = new MilHistProjectTemplate (page);
            }
            return projectTemplate;
        }
		set => projectTemplate = value;
    }

    private void initMilHistTemplateNameRegex ()
    {
    	// Regenerate from the redirects
        var templatePage = new Page (page.Bot, "Template:WikiProject Military history");
        var redirects = templatePage.Redirects ();
        List<string> s = new List<string>();
        foreach (var redirect in redirects)
        {
            s.Add(redirect.TitleWithoutNs ());
        }
        string milHistTemplateNameRegex2 = String.Join ("|", s);
        Console.WriteLine (milHistTemplateNameRegex2);
    }

    public static bool IsMilHist (Template template)
    {
        return template.Name.Matches (milHistTemplateNameRegex);
    }

    public static bool IsMilHist (Page page)
    {
        Page talk = page.Talk;
        talk.Load ();
        return talk.Exists (milHistTemplateNameRegex);
    }

    public bool IsCoordinator (string name)
    {
    	if (null == coordinators)
    	{
        	coordinators = new Coordinators (page.Bot, "MILHIST");
        }
        return coordinators.IsCoordinator (name);
    }

    public MilHist (Page page)
    {
        this.page = page;
    }
}

public class Rating
{
    private Page talk;
    private Page article;

    public string Class       { get; set; }
    public bool HasReferences { get; set; }
    public bool HasCoverage   { get; set; }
    public bool HasStructure  { get; set; }
    public bool HasGrammar    { get; set; }
    public bool HasSupport    { get; set; }

    private const string addenda = "Notes|References|External links|Further reading|See Also|Bibliography|Citations";

    private bool unsourced (Page article)
    {
        return article.InCategory ("Category:All articles with unsourced statements");
    }

    private string listRating (string rating)
    {
        switch (rating)
        {
            case "Start":
            case "Stub":
                rating = "List";
                break;
            case "C":
               rating = "CL";
                break;
            case "B":
               rating = "BL";
                break;
            default:
                throw new ApplicationException ("unknown rating: '" + rating + "'");
        }
        return rating;
    }

    private List<IToken> getLineTokens (Page article)
    {
        int firstIndex = 0;

        int lastIndex = article.FindLastIndex ();
        if (lastIndex < 1)
        {
        	return new List<IToken>();
        }

        var firstSection = article.FindSection (".");
        if (null != firstSection)
        {
            firstIndex = article.FindIndex (firstSection) + 1;
        }

        var lastSection  = article.FindSection (addenda);
        if (null != lastSection)
        {
            lastIndex  = article.FindIndex (lastSection);
        }

        if (lastIndex <= firstIndex)
        {
        	firstIndex = 0;
        }

        var lineTokens = article.GetRange (firstIndex, lastIndex - firstIndex);

        return lineTokens;
    }

    private bool inlineCitations (Page article)
    {
    	Debug.Entered ();
        bool isReferenced = true;
        if (unsourced (article))
        {
            isReferenced = false;
        }
        else
        {
        	var count = article.Sections.Count - article.FindAllSections (addenda).Count;
        	if (count <= 0)
        	{
            	isReferenced = false;
        	}
        	else if (article.Exists (@"^(Verify|" +
						"Not verified|" +
						"Cleanup-verify|" +
						"Notverified|" +
						"Cite sources|" +
						"Sources|" +
						"More sources|" +
						"Citations missing|" +
						"Referenced|" +
						"Citations needed|" +
						"Moresources|" +
						"Missing citations|" +
						"Morerefs|" +
						"Morereferences|" +
						"Moreref|" +
						"Fewreferences|" +
						"Cleanup-cite|" +
						"Cleanup cite|" +
						"Few references|" +
						"More citations needed|" +
						"More references|" +
						"Improve-refs|" +
						"Ref-improve|" +
						"Ref improve|" +
						"Improve references|" +
						"Improvereferences|" +
						"Improverefs|" +
						"Improve refs|" +
						"RefImprove|" +
						"Verification|" +
						"Additionalcitations|" +
						"Additional citations|" +
						"Improveref|" +
						"Fewrefs|" +
						"Few refs|" +
						"More refs|" +
						"Ref Improve|" +
						"Reference improve|" +
						"Refimproved|" +
						"Needs more references|" +
						"Citationsneeded|" +
						"Refim|" +
						"Add references|" +
						"Addref|" +
						"Rip|" +
						"Improve-references|" +
						"Few sources)$"))
        	{
        		Debug.WriteLine ("Matched improve references template");
        		isReferenced = false;
        	}
        	else
        	{
	            var lineTokens = getLineTokens (article);
	            var lines      = lineTokens.FindAll (ind=>(ind.Type == TokenType.Newline));

	            foreach (var line in lines)
	            {
	                IToken last;
	                int index = article.FindIndex (line);
	                if (index <= 0)
	                {
	                    isReferenced = false;
	                    break;
	                }
	                do
	                {
	                    last  = article.GetToken (--index);
					} while (index > 0 && (last.Type == TokenType.Newline || last.Type == TokenType.Comment || last.Type == TokenType.Table));

	                if (index > 0 && last.Type != TokenType.Section && last.Type != TokenType.Reference && last.Type != TokenType.Template && last.Type != TokenType.Link && last.Type != TokenType.Colon && last.Type != TokenType.Blockquote && !String.IsNullOrWhiteSpace (last.Text))
	                {
	                    Debug.WriteLine ("Unreferenced = '" + last.Text + "' type = " + last.Type);
	                    isReferenced = false;
	                    break;
	                }
	            }
	        }
        }
        Debug.Exited ();

        return isReferenced;
    }

    private bool coverage (Page article)
    {
    	bool result = false;
        if (article.Exists ("Empty section"))
        {
        	Debug.WriteLine ("\tEmpty section");
        }
        else
        {
			var bytesCount = article.BytesCount ();
			if (bytesCount < 1500)
        	{
        		Debug.WriteLine ("\tToo short -" + bytesCount + "bytes");
        	}
        	else
        	{
        		var prediction = article.Prediction ();
				Debug.WriteLine ("\tprediction = " + prediction);
        		result = ! (prediction.Equals ("Start") || prediction.Equals ("Stub"));
        	}
        }
        return result;
    }

    private bool structure (Page article)
    {
        return article.Sections.Count > article.FindAllSections (addenda).Count;
    }

    private bool grammar ()
    {
        return true;
    }

    private bool support (Page article)
    {
        return article.Exists ("Infobox") || article.ExistsNs ("File");
    }

    private string rating ()
    {
		var bytesCount = article.BytesCount ();
        string rating = article.IsRedirect       ? "Redirect" :
        				article.IsProject        ? "Project"  :
        				article.IsDisambiguation ? "Disambig" :
        				((HasReferences && HasCoverage) && HasStructure && HasGrammar && HasSupport) ? "B" :
                        ((HasReferences || HasCoverage) && HasStructure && HasGrammar && HasSupport && bytesCount > 1500) ? "C" :
                        article.Prediction ().Equals ("Stub") ? "Stub" :
	                	"Start";

        bool isList = talk.MilHist.ProjectTemplate.IsList;

        if (isList)
        {
            rating = listRating (rating);
        }
        return rating;
    }

    public Rating (Page page)
    {
        talk          = page.Talk;
        article       = page.Article;

        if (! (article.IsRedirect || article.IsProject || article.IsDisambiguation))
        {
	        HasReferences = inlineCitations (article);
	        HasCoverage   = coverage (article);
	        HasStructure  = structure (article);
	        HasGrammar    = grammar ();
	        HasSupport    = support (article);
	    }
        Class = rating ();
    }
}

public class TaskForces
{
    private static Dictionary<string, string> dictionary;
    public HashSet<string> TaskForceList { get; private set; }

    public bool Any ()
    {
        return TaskForceList.Count > 0;
    }

    public override string ToString ()

    {
        return String.Join (", ", TaskForceList.ToList ());
    }

    private bool isMaintenance (string category)
    {
		const string maintenance = "CS1 maintenance|Commons category link is on Wikidata|(Hidden|Tracking|Wikipedia) (articles|categories)|CatAutoTOC|Pages with|Template Large category|Use dmy|Use mdy";
		Match match = Regex.Match (category, maintenance, RegexOptions.IgnoreCase);
		return match.Success;
    }

    public TaskForces (Page article)
    {
        TaskForceList = new HashSet<string>();

		// Gets all parent categories as well
        var categories = article.Categories ().FindAll (ind => ! isMaintenance (ind));
        var parents = new List<string>();
        foreach (var category in categories)
		{
		    var parent = new Page (article.Bot, category);
		    parents.AddRange(parent.Categories ().FindAll (ind => ! isMaintenance (ind)));
	    }
		categories.AddRange (parents);

        foreach (var category in categories)
		{
	    	Debug.WriteLine (category);
            foreach (KeyValuePair<string, string> pair in dictionary)
            {
                Match match = Regex.Match (category, pair.Key, RegexOptions.IgnoreCase);
                if (match.Success)
                {
	    	    	Debug.WriteLine (category);
	            	Debug.WriteLine ("    Found (" + pair.Value + ") " +  category);
                    TaskForceList.Add (pair.Value);
                }
            }
        }
        if (TaskForceList.Contains ("Muslim") && ! TaskForceList.Contains ("Medieval"))
        {
			Debug.WriteLine ("    Removing 'Muslim'");
			TaskForceList.Remove ("Muslim");
        }
    }

    static TaskForces ()
    {
        dictionary = new Dictionary<string, string>()
		{
		    // General topics
		    { "Aviation|Aircraft|Aerospace|Air Force", "Aviation" },
		    { @"Births|Deaths|People(?!(s|'))", "Biography" },
		    { @"($<!Recipients.+)(Awards|Badges|Bands|Customs|Decorations|Emblems|Flags|Holidays|Insignia|Ranks)", "Culture" },
		    { "Films", "Films" },
		    { "Castle|Fortification|Gatehouse|Tower", "Fortifications" },
		    { "History books|Historians", "Historiography" },
		    { "Logistics", "Logistics" },
		    { "Maritime|Navy|Naval|Ships", "Maritime" },
		    { "Memorial", "Memorials" },
		    { @"Muslim|Islam(?!(ic (Republic|State)))", "Muslim" },
		    { "Formations|Units|Military of", "National" },
		    { "Science|Technology|Military (Electronics|Equipment)", "SciTech" },
		    { "Ammunition|Armour$|Weapon", "Weaponry" },

		    // nations and regions
		    { "Africa|Ethiopia|Eritrea|Commonwealth War Graves", "African" },
		    { "China|Chinese", "Chinese" },
		    { @"Russia(?<!Prussia)|Belarus|Ukraine|Armenia|Soviet Union",  "Russian" },
		    { "Indian|Commonwealt War Graves",  "Indian" },
		    { "Pakistan|Nepal|Bangladesh|Sri Lanka",  "South-Asian" },
		    { "Italy|Italian(?!-language)",  "Italian" },
		    { "Japan",  "Japanese" },
		    { "Poland|Polish",  "Polish" },
		    { "($<!Wikiproject )Spain|Portugal|Spanish(?!(-language| Wikipedia))",   "Spanish" },
		    { "United States", "US" },
		    { "Netherlands|Dutch(?!(-language| Wikipedia))", "Dutch" },
		    { "Ottoman|Turkish", "Ottoman" },
		    { "France|French(?!.language)", "French" },
		    { @"German(?!(-language| Wikipedia))|Prussia", "German" },
		    { "Australia|New Zealand|Commonwealth War Graves", "ANZSP" },
		    { "Britain|British|United Kingdom|Commonwealth War Graves", "British" },
		    { "Canada|Canadian|Commonwealth War Graves", "Canadian" },
		    { "Burma|Indonesia|Cambodia|Laos|Malaysia|Singapore|Thai|Timor|Vietnam|Southeast Asia|South East Asia", "Southeast-Asian" },
		    { "Korea", "Korean" },
		    { "Swedish|Sweden|Denmark|Danish|Norway|Norweigan|Finland|Finnish|Iceland|Viking", "Nordic" },
		    { "Lithuania|Latvia|Estonia|Baltic", "Baltic" },
		    { "Iran|Arabia|Arab Emirates|Algeria|Egypt|Israel|Jordan|Syria|Palestin|Middle East|Persia", "Middle-Eastern" },
		    { "Albania|Balkan|Bosnia|Bulgaria|Croatia|Greece|Greek|Kosovo|Montenegro|Romania|Serbia|Yugoslav", "Balkan" },
		    { "Argentina|Bolivia|Brazil|Columbia|Ecuador|Falklands|Malvinas|Paraguay|Uraguay|Venezuala|South America", "South-American" },
		    { "Mexic|Central America", "Latin-American" },

		    // Periods and conflicts
		    { "191[4-9] (?!(births|deaths|.*books))|World War I(?!I)", "WWI" },
		    { "1939|194[0-5] (?!(births|deaths|.*books))|World War II", "WWII" },
		    { "19[5-8]\\d (?!(births|deaths|.*books))|Cold War", "Cold-War" },
		    { "(199\\d|20\\d\\d) (?!(births|deaths|.*books))", "Post-Cold-War" },
		    { "Crusade", "Crusades" },
		    { "Ancient|Classical", "Classical" },
		    { "Napoleon", "Napoleonic" },
		    { "American Civil War", "ACW" },
		    { "Byzantine|($<!Holy )Roman(?!(ia| Catholic))", "Roman" },
		    { @"Medieval|:(5|6|7|8|9|1[0-4])th( |-)century|(1[0-4])\d\d", "Medieval" },
		    { "1[5-8]th( |-)century", "Early-Modern" },
	    };
	// dictionary.TrimExcess ();
	}
}

public class MilHistProjectTemplate : Template
{
    private bool flag (string key)
    {
        if (Exists (key))
        {
            var p = Find (key);
	    	return p.Value.Equals ("y") || p.Value.Equals ("yes");
        }
        return false;
    }

    public static string FirstCharToUpper (string input)
    {
        return String.IsNullOrEmpty (input) ? "" : input.Substring(0,1).ToUpper() + input.Substring(1).ToLower ();
    }

    public string Class
    {
        get => Exists ("^class$") && ! Find ("^class$").Value.Equals ("") ? FirstCharToUpper (Find ("^class$").Value) : "unclassified";
        set => Add ("class", value);
    }

    public bool IsList
    {
        get => flag ("list");
        set => Add ("list", "yes");
    }

    public bool HasReferences
    {
        get => flag ("b1");
        set => Add ("b1", value);
    }

    public bool HasCoverage
    {
        get => flag ("b2");
        set => Add ("b2", value);
    }

    public bool HasStructure
    {
        get => flag ("b3");
        set => Add ("b3", value);
    }

    public bool HasGrammar
    {
        get => flag ("b4");
        set => Add ("b4", value);
    }

    public bool HasSupport
    {
        get => flag ("b5");
        set => Add ("b5", value);
    }

    public bool Missing ()
    {
    	return FindAll (@"b\d").Count < 5;
    }

    public Rating Rating
    {
        get => new Rating (Page);
        set
        {
            Class = value.Class;
            if (! Class.Matches ("Disambig|Project|Redirect|Stub") )
            {
	            HasReferences = value.HasReferences;
	            HasCoverage   = value.HasCoverage;
	            HasStructure  = value.HasStructure;
	            HasGrammar    = value.HasGrammar;
	            HasSupport    = value.HasSupport;
	        }
        }
    }

    public void Add (TaskForces taskForces)
    {
		var taskForceList = taskForces.TaskForceList;
        foreach (var taskForce in taskForceList)
        {
            Add (taskForce, "yes");
        }
    }

    public MilHistProjectTemplate (Template template) : base (template)
    {
        Page.Templates.Add (this);
    }

    public MilHistProjectTemplate (Page page) : base ("WikiProject Military history\n")
    {
        var last = Page.FindLast ("WikiProject");
        if (last == null)
        {
            page.Insert (Page.Top (), this, new Token ("\n"));
        }
        else
        {
            page.Insert (last, this, new Token ("\n"));
        }
    }
}


using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class Template : IToken
{
    public string Name         { get; set; }
    public Namespace Namespace { get; set; }
    public Page Page           { get; set; }
    public TokenType Type      { get; private set; }
    public string Text         { get { return text (); } }
    public bool Deleted        { get; set; }
    public bool Magic          { get; set; }
    public Template Owner      { get; set; }
    public Tokens Contents     { get; set; }

    private Tokens name;
    private List<Parameter> parameters;
    private int defaultCount;

	public string this [ string index ]
	{
		get
		{
			return Exists (index) ? Find (index).Value : null;
		}
		set
		{
			Add (index, value);
		}
	}
	
    private string text ()
    {
        if (Deleted)
        {
            return string.Empty;
		}
		
        var tokenStringList = new List<string>();
        tokenStringList.Add ("{{");
        if (null != Namespace)
        {
            tokenStringList.Add (Namespace.Text);
            tokenStringList.Add (":");
        }
        tokenStringList.Add (name.Text);

        int count = 0;
        if (Magic)
        {
           tokenStringList.Add (":");
        }
        foreach (var parameter in parameters)
        {
            if (! (Magic && 0 == count))
            {
                tokenStringList.Add ("|");
            }
            tokenStringList.Add (parameter.Text);
            ++count;
        }
        tokenStringList.Add ("}}");
        string tokenString = String.Join (string.Empty, tokenStringList);
        return tokenString;
    }

    private Tokens removeComments (Tokens tokens)
    {
        return new Tokens (tokens.Contents.FindAll (ind=>(ind.Type != TokenType.Comment)));
    }

    private Tokens parameterName (Tokens queue)
    {
        var tokens = new Tokens ();
        while (queue.Any () && ! queue.IsToken (TokenType.Equals) && ! queue.IsToken (TokenType.Pipe))
        {
            tokens.Add (queue.Dequeue ());
        }
        return tokens;
    }

    private Tokens templateName (Tokens queue)
    {
        var tokens = new Tokens ();
        while (queue.Any () && ! queue.IsToken (TokenType.Colon) && ! queue.IsToken (TokenType.Pipe))
        {
            tokens.Add (queue.Dequeue ());
        }
        return tokens;
    }

    private Tokens parameterValue (Tokens queue)
    {
        var tokens = new Tokens ();
        while (queue.Any () && ! queue.IsToken (TokenType.Pipe))
        {
            tokens.Add (queue.Dequeue ());
        }
        return tokens;
    }

    private void parse (Tokens tokens)
    {
        Tokens queue = tokens.Clone ();
        name = templateName (queue);
        Name = removeComments (name).Text.Trim ();

        if (queue.IsToken (TokenType.Colon))
        {
            queue.Dequeue ();
            if (Namespace.IsNamespace (Name))
            {
                Namespace = new Namespace (Name);
                name = parameterName (queue);
                Name = removeComments (name).Text.Trim ();
            }
            else
            {
                Magic = true;
                if (queue.Any ())
                {
                    parameters.Add (new Parameter (0, queue));
                    return;
                }
            }
        }

        while (queue.Any ())
        {
            if (queue.IsToken (TokenType.Pipe))
            {
                queue.Dequeue ();
                var parameterTokens = parameterName (queue);
                if (queue.IsToken (TokenType.Equals))
                {
                    queue.Dequeue ();
                    parameters.Add (new Parameter (parameterTokens, parameterValue (queue)));
                }
                else
                {
                    parameters.Add (new Parameter (++defaultCount, parameterTokens));
                }
            } else {
                throw new ParseException ("unexpected token: '" + queue.Contents[0].Text + "'");
            }
        }
    }

    public void Add (Parameter parameter)
    {
        int pos = FindIndex (parameter);
        if (pos >= 0)
        {
            parameters[pos] = parameter;
        }
        else
        {
            parameters.Add (parameter);
        }
    }

    public void Add (string name, string value)
    {
        Add (new Parameter (name, value));
    }

    public void Add (string name, bool value)
    {
        Add (name, value ? "yes" : "no");
    }

    public void Add (int id, string value)
    {
        Add (new Parameter (id, value));
    }

    public bool Exists (string match)
    {
        return parameters.Exists (ind=>Regex.IsMatch (ind.Name, match, RegexOptions.IgnoreCase));
    }

    public bool Exists (string match, string value)
    {
        return parameters.Exists (ind=>Regex.IsMatch (ind.Name, match, RegexOptions.IgnoreCase) && Regex.IsMatch (ind.Value, value, RegexOptions.IgnoreCase));
    }

    public int FindIndex (Parameter parameter)
    {
        return parameters.FindIndex (ind=>parameter.Name.Equals (ind.Name, StringComparison.OrdinalIgnoreCase));
    }

    public Parameter Find (string match)
    {
        return parameters.Find (ind=>Regex.IsMatch (ind.Name, match, RegexOptions.IgnoreCase));
    }

    public List<Parameter> FindAll (string match)
    {
        return parameters.FindAll (ind=>Regex.IsMatch (ind.Name, match, RegexOptions.IgnoreCase));
    }

    public List<Parameter> FindAll (string match, string value)
    {
        return parameters.FindAll (ind=>Regex.IsMatch (ind.Name, match, RegexOptions.IgnoreCase) && Regex.IsMatch (ind.Value, value, RegexOptions.IgnoreCase));
    }

    public Parameter FindLast (string match)
    {
        return parameters.FindLast (ind=>Regex.IsMatch (ind.Name, match, RegexOptions.IgnoreCase));
    }

    public Parameter FindLast (string match, string value)
    {
        return parameters.FindLast (ind=>Regex.IsMatch (ind.Name, match, RegexOptions.IgnoreCase) && Regex.IsMatch (ind.Value, value, RegexOptions.IgnoreCase));
    }

    public void Insert (int index, Parameter parameter)
    {
        parameters.Insert (index, parameter);
    }

    public void Insert (int index, string name, string value)
    {
        Insert (index, new Parameter (name, value));
    }

    public void Remove (Parameter parameter)
    {
        parameters.Remove (parameter);
    }

    public void RemoveAll (string match)
    {
        var parameters = FindAll (match);
        foreach (var parameter in parameters)
        {
            Remove (parameter);
        }
    }

    public void Overwrite (Template t)
    {
        name = t.name;
        Name = t.Name;
        Namespace = t.Namespace;
        Page = t.Page;
        Type = t.Type;
        Deleted = t.Deleted;
        Magic = t.Magic;
        parameters = t.parameters;
        defaultCount = t.defaultCount;
    }

    public override string ToString ()
    {
        return text();
    }

    private void init ()
    {
        Type = TokenType.Template;
        defaultCount = 0;
        Deleted = false;
        Magic = false;
        Namespace = null;
		Owner = null;
        parameters = new List<Parameter>();
    }

    public Template (Page page, Tokens tokens)
    {
        Page = page;
        Tokens contents = new Tokens ();
        while (! tokens.Empty ())
        {
            var token = tokens.Dequeue ();
            try
            {
                switch (token.Type)
                {
                    case TokenType.OpenComment:
                        contents.Add (new Comment (page, tokens));
                        break;
                    case TokenType.OpenGallery:
                        contents.Add (new Gallery (page, token, tokens));
                        break;
                    case TokenType.OpenNowiki:
                        contents.Add (new Nowiki (page, tokens));
                        break;
                    case TokenType.OpenBracket:
                        contents.Add (new Link (page, tokens));
                        break;
                    case TokenType.Reference:
                        contents.Add (new Reference (page, token));
                        break;
                    case TokenType.OpenReference:
                        contents.Add (new Reference (page, token, tokens));
                        break;
                    case TokenType.OpenTemplate:
						var template = page.SpecialTemplates (new Template (page, tokens));
						template.Owner = this;
                        contents.Add (template);
                        break;
                    case TokenType.CloseTemplate:
                        init ();
                        parse (contents);
                        Contents = contents;
                        page.Templates.Add (this);
                        return;
                    default:
                        contents.Add (token);
                        break;
                }
            }
            catch (ParseException)
            {
                contents.Add (token);
            }
        }
        tokens.InsertRange (0, contents);
        throw new ParseException ("Unclosed template on " + page.Title + ": " + contents.Preview ());
    }

    public Template (Tokens contents)
    {
        init ();
        parse (contents);
        Contents = contents;
    }

    public Template (string value) : this (new Tokens (value))
    {
    }
    
    public Template (Template template)
    {
		Overwrite (template);
    }

}

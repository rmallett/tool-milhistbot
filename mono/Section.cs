using System;

public class Section : IToken
{
    public Page Page       { get; set; }
    public TokenType Type  { get; set; }
    public string Text     { get; set; }
    public string Title    { get; set; }    
    public int Level       { get; set; }
    public string Number   { get; set; }
    public Tokens Contents { get; set; }

    public override string ToString ()
    {
        return Text;
    }

    public void Add (params IToken [] items)
    { 
        Contents.Contents.AddRange (items);
    }

    public void Add (string s)
    { 
        Add (new Tokens (s));
    }

    public void Add (Section section)
    { 
        Add (section.Contents);
    }

    public void Save (string summary)
    {
        Page.Bot.Save (this, summary);
        if (Number.Equals ("new"))
        {
            int number = Page.Sections.Count - 1;
            Number = number.ToString ();
        }
    }
    
    public Section (Page page, Tokens tokens) 
    {
        Tokens contents = new Tokens ();
        while (! tokens.Empty ()) 
        {
            var token = tokens.Dequeue ();
            switch (token.Type)
            {
                case TokenType.MultiEquals:
                    Page   = page;
                    Title  = contents.Text.Trim ();
                    Level  = token.Text.Length;
                    Text   = token.Text + contents.Text + token.Text;
                    Contents = new Tokens ();                   
                    Type   = TokenType.Section;
                    Number = page.Sections.Count.ToString ();
                    page.Sections.Add (this);
                    return;
                default: 
                    contents.Add (token);
                    break;
            }
        }
    }

    public Section (Page page, string heading, int level) 
    {
        const string s = "========"; 
        Page   = page;      
        Title  = heading;
        Level  = level;
        Text   = s.Substring (0, level) + " " + heading + " " + s.Substring (0, level);
        Type   = TokenType.Section;
        Contents = new Tokens ();
        Number = "new"; 
        page.Sections.Add (this);
    }
    
    public Section (Page page, string heading) : this (page, heading, 2)
    {
    }
}
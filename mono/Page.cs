using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

public class ParseException : Exception
{
    public ParseException(string message) : base (message)
    {
    }
}

public class Page
{
    public Bot Bot            		{ get; set; }
    public int Ns 					{ get; set; }
    public int PageId				{ get; set; }
    public bool ReadOnly 			{ get; set; }
	public int RevId 				{ get; set; }
	public int OldRevId				{ get; set; }
    public List<Template> Templates { get; set; }
    public List<Section> Sections 	{ get; set; }
    public List<Link> Links 		{ get; set; }
    public ArticleHistory ArticleHistory { get; set; }
    public Query Query  			{ get; set; }
    public MilHist MilHist 			{ get; set; }

    private List<string> categories;
    private string _namespace;
    private string _title;
    private Page _talk;
    private Page _article;
    private Tokens _tokens;
    private string prediction;
    private bool checkedRedirect;
    private Page redirectsTo;


    public string Content {
        get => _tokens.Text;
        set => parse (value);
    }

    public Page Article
    {
        get
        {
            if (null == _article)
            {
			    setArticleAndTalk ();
            }
            return _article;
        }
        set => _article = value;
    }

    public bool IsDisambiguation
    {
        get
        {
        	return InCategory ("disambiguation");
        }
    }

    public bool IsProject
    {
        get
        {
            return Namespace.Equals ("Wikipedia");
        }
    }

    public bool IsRedirect
    {
        get
        {
            return null != RedirectsTo;
        }
    }

    public string Namespace
    {
        get
        {
            if (null == _namespace)
            {
                if (null == Title)
                {
                    return "";
                }
                else
                {
                    Match match = Regex.Match (Title, @"^.+?:");
                    _namespace = match.Success ? Title.Substring (0, match.Length - 1) : "";
                }
            }
            return _namespace;
        }
        set => _namespace = value;
    }

    public Page RedirectsTo
    {
        get
        {
            if (! checkedRedirect)
            {
                redirectsTo = Bot.RedirectsTo (Title);
                checkedRedirect = true;
            }
            return redirectsTo;
        }
    }

    public Page Talk
    {
        get
        {
            if (null == _talk)
            {
				setArticleAndTalk ();
			}
            return _talk;
        }
        set => _talk = value;
    }

    public string Title
    {
        get => _title;
        set
        {
            _title = value;
            _namespace = null;
            _talk = null;
        }
    }

    public string TitleWithoutNs ()
    {
        int pos = Title.IndexOf (':', 1);
        string titleWithoutNs =  (pos < 0) ? Title : Title.Substring (pos + 1);
        return titleWithoutNs;
    }

	public bool IsTalk ()
	{
        Match match = Regex.Match (Namespace, "Talk", RegexOptions.IgnoreCase);
		return match.Success;
	}

	private void setArticleAndTalk ()
	{
		// Console.WriteLine ("setArticleAndTalk: title='" + Title + "'");
        if (IsTalk ())
        {
        	var articleTitle =  Namespace.Equals ("Talk") ? Title.Substring (5) : (Namespace.Substring (0, Namespace.Length - 5) + ":" + TitleWithoutNs ());
            _article = new Page (Bot, articleTitle);
            _talk = this;
		    _article.Article = _article;
		    _article.Talk = _talk;
        }
        else
        {
        	var talkTitle = Namespace.Equals ("") ? ("Talk:" + Title) : (Namespace + " talk:" + Title);
            _article = this;
            _talk = new Page (Bot, talkTitle);
		    _talk.Article = _article;
		    _talk.Talk = _talk;
        }
		// Console.WriteLine ("setArticleAndTalk: article='" + _article.Title + "' talk='" + _talk.Title + "'");
	}


    public override string ToString ()
    {
        return Content;
    }

    public Template SpecialTemplates (Template template)
    {
    	// Debug.Entered ("template=" + template.Name);
        if (ArticleHistory.IsArticleHistory (template))
        {
            ArticleHistory = new ArticleHistory (template);
            return ArticleHistory;
        }
        else if (ArticleForDeletion.IsArticleForDeletion (template))
        {
            return new ArticleForDeletion (template);
        }
        else if (DidYouKnow.IsDidYouKnow (template))
        {
            return new DidYouKnow (template);
        }
        else if (GoodArticle.IsGoodArticle (template))
        {
            return new GoodArticle (template);
        }
        else if (InTheNews.IsInTheNews (template))
        {
            return new InTheNews (template);
        }
        else if (OldPeerReview.IsOldPeerReview (template))
        {
            return new OldPeerReview (template);
        }
        else if (OnThisDay.IsOnThisDay (template))
        {
            return new OnThisDay (template);
        }
        else if (Nomination.IsNomination (template))
        {
        	return new Nomination (template);
        }
        else if (MilHist.IsMilHist (template))
        {
        	MilHist.ProjectTemplate = new MilHistProjectTemplate (template);
	        return MilHist.ProjectTemplate;
        }

		return template;
    }

    private void parse (string content)
    {
        _tokens = new Tokens();
        Tokens tokens = new Tokens (content);
        while (! tokens.Empty ())
        {
            var token = tokens.Dequeue ();
            try
            {
                switch (token.Type)
                {
                    case TokenType.OpenComment:
                        _tokens.Add (new Comment (this, tokens));
                        break;
                    case TokenType.OpenNowiki:
                        _tokens.Add (new Nowiki (this, tokens));
                        break;
                    case TokenType.OpenBracket:
                        _tokens.Add (new Link (this, tokens));
                       break;
                    case TokenType.OpenExternalLink:
                        _tokens.Add (new ExternalLink (this, tokens));
                       break;
                    case TokenType.OpenTemplate:
                        _tokens.Add (SpecialTemplates (new Template (this, tokens)));
                        break;
                    case TokenType.OpenBlockquote:
                        _tokens.Add (new Blockquote (this, tokens));
                        break;
                    case TokenType.Reference:
                        _tokens.Add (new Reference (this, token));
                        break;
                    case TokenType.OpenReference:
                        _tokens.Add (new Reference (this, token, tokens));
                        break;
                    case TokenType.OpenTable:
                        _tokens.Add (new Table (this, tokens));
                        break;
                    case TokenType.OpenGallery:
                        _tokens.Add (new Gallery (this, token, tokens));
                        break;
                    case TokenType.MultiEquals:
                        _tokens.Add (new Section (this, tokens));
                        break;
                    default:
                        _tokens.Add (token);
                        break;
                }
            }
            catch (ParseException)
            {
                _tokens.Add (token);
            }
        }
    }

    public void Add (params IToken [] items)
    {
        _tokens.Contents.AddRange (items);
        foreach (IToken item in items)
        {
            if (item.Type == TokenType.Template)
            {
                Templates.Add ((Template)item);
            }
        }
    }

    public void Add (string s)
    {
        Add (new Tokens (s));
    }

    public bool Exists (string match)
    {
        return Templates.Exists (ind=>Regex.IsMatch (ind.Name, match, RegexOptions.IgnoreCase));
    }

    public Template Find (string match)
    {
        return Templates.Find (ind=>Regex.IsMatch (ind.Name, match, RegexOptions.IgnoreCase));
    }

    public Template FindLast (string match)
    {
        return Templates.FindLast (ind=>Regex.IsMatch (ind.Name, match, RegexOptions.IgnoreCase));
    }

    public List<Template> FindAll (string match)
    {
        return Templates.FindAll (ind=>Regex.IsMatch (ind.Name, match, RegexOptions.IgnoreCase));
    }

    public Comment FindComment (string match)
    {
        return (Comment) _tokens.Contents.Find (ind=>(ind.Type == TokenType.Comment && Regex.IsMatch (((Comment)ind).Value, match, RegexOptions.IgnoreCase)));
    }

    public bool ExistsNs (string ns)
    {
        return _tokens.Contents.Exists (ind=>(ind.Type == TokenType.Link && ns.Equals (((Link)ind).Namespace.Text)));
    }

    public Link FindNs (string ns)
    {
        return (Link) _tokens.Contents.Find (x=>(x.Type == TokenType.Link && ns.Equals (((Link)x).Namespace.Text)));
    }

    public List<Link> FindAllNs (string ns)
    {
         var tokenList = _tokens.Contents.FindAll (x=>(x.Type == TokenType.Link && ns.Equals (((Link)x).Namespace.Text)));
         return tokenList.ConvertAll(x => (Link)x);
    }

    public Section FindSection (string match)
    {
        return Sections.Find (ind=>(Regex.IsMatch (ind.Title, match, RegexOptions.IgnoreCase)));
    }

    public List<Section> FindAllSections (string match)
    {
        return Sections.FindAll (ind=>(Regex.IsMatch (ind.Title, match, RegexOptions.IgnoreCase)));
    }

    public IToken FindText (string match)
    {
        return _tokens.Contents.Find (ind=>(ind.Type == TokenType.Text && Regex.IsMatch (ind.Text, match, RegexOptions.IgnoreCase)));
    }

    public List<IToken> FindAllText (string match)
    {
        return _tokens.Contents.FindAll (ind=>(ind.Type == TokenType.Text && Regex.IsMatch (ind.Text, match, RegexOptions.IgnoreCase)));
    }

    public int FindIndex (IToken token)
    {
        return _tokens.Contents.FindIndex (x => x == token);
    }

    public int FindLastIndex ()
    {
        return _tokens.Contents.Count - 1;
    }

    public List<IToken> GetRange (int index, int count)
    {
        return _tokens.Contents.GetRange (index, count);
    }

    public IToken GetToken (int index)
    {
        return _tokens.Contents.ElementAt (index);
    }

    public void Insert (IToken token, string s)
    {
        _tokens.Contents.Insert (FindIndex (token), new Token (s));
     }

    public void Insert (IToken token, params IToken [] items)
    {    	
        int pos = FindIndex (token);
        _tokens.Contents.InsertRange (pos, items);
        foreach (IToken item in items)
        {
            if (item.Type == TokenType.Template)
            {
                Templates.Add ((Template)item);
            }
        }
    }

    public bool Remove (Template template)
    {
        int pos = _tokens.Contents.FindIndex (x => x == template);
        if (pos >= 0)
        {
            _tokens.Contents.Remove (template);
            if (pos < _tokens.Contents.Count)
            {
                var p = _tokens.Contents[pos];
                if (String.IsNullOrWhiteSpace (p.Text))
                {
                    _tokens.Contents.Remove (p);
                }
            }
            return Templates.Remove (template);
        }
        return false;
    }

    public void RemoveRange (int index, int count)
    {
        List<IToken> items = _tokens.Contents.GetRange (index, count);
        foreach (IToken item in items)
        {
            if (item.Type == TokenType.Template)
            {
                Templates.Remove ((Template)item);
            }
        }
        _tokens.Contents.RemoveRange (index, count);
    }

    public void Replace (Template template1, Template template2)
    {
        template1.Overwrite (template2);
    }

    public void Replace (string pattern, string replacement)
    {
        Regex regex = new Regex (pattern, RegexOptions.Multiline);
        Content = regex.Replace (Content, replacement);
    }

    public IToken Top ()
    {
        return _tokens.Contents[0];
    }

    public IToken EndOfLine (IToken token)
    {
        var index = FindIndex (token);
        while (_tokens.Contents[index].Type != TokenType.Newline)
        {
            ++index;
        }
        return _tokens.Contents[index];
    }

    public IToken StartOfLine (IToken token)
    {
        var index = FindIndex (token);
        while (_tokens.Contents[index].Type != TokenType.Newline)
        {
            --index;
        }
        return _tokens.Contents[index+1];
    }
    
    public bool InCategory (string match)
    {
        return Categories ().Exists (ind=>Regex.IsMatch (ind, match, RegexOptions.IgnoreCase));
    }

    public List<string> Categories ()
    {
        if (null == categories)
        {
            categories = Bot.Categories (Title);
        }
        return categories;
    }

    public void LoadFromStrings (string content)
    {
        Content = content;
    }

    public void LoadFromFile (string path)
    {
        Content = File.ReadAllText (path, Encoding.UTF8);
    }

    public void SaveToFile (string path)
    {
        File.WriteAllText (path, Content);
    }

    public void Load ()
    {
        Bot.Load (this, RevId);
    }

    public void Save (string summary)
    {
        Bot.Save (this, summary);
    }

    public void Save (Section section, string summary)
    {
        Bot.Save (section, summary);
    }

    public string Prediction ()
    {
        if (null == prediction)
        {
            prediction = Bot.Prediction (Title);
        }
        return prediction;
    }

    public List<Revision> History ()
    {
        if (null == Query)
        {
            Query = new Query (Title);
        }
        return Bot.History (Query);
    }

    public List<Revision> History (int limit)
    {
        if (null == Query)
        {
            Query = new Query (Title, limit);
        }
        return Bot.History (Query);
    }

    public List<Page> Redirects ()
    {
        var query = new Query (Title);
 	      var redirects = new List<Page>();
        do
        {
            List<Page> batch = Bot.Redirects (query);
	          redirects.AddRange (batch);
        } while (query.Continue);
        return  redirects;
    }

    public int BytesCount ()
    {
        int count = 0;
        foreach (var token in _tokens.Contents)
        {
            switch (token.Type)
            {
                case TokenType.Comment:
                case TokenType.ExternalLink:
                case TokenType.Reference:
                case TokenType.Section:
                case TokenType.Table:
                case TokenType.Template:
                    break;
                case TokenType.Link:
                    Link link = (Link)token;
                    if (link.Namespace.Text.Equals (""))
                    {
                        count += link.Value.Length;
                    }
                    break;
                default:
    	            if (String.IsNullOrWhiteSpace (token.Text))
                        break;
                    count += token.Text.Length;
                    break;
            }
        }
        return count;
    }

    public int WordsCount ()
    {
        int count = 0;
        foreach (var token in _tokens.Contents)
        {
            switch (token.Type)
            {
                case TokenType.Comment:
                case TokenType.ExternalLink:
                case TokenType.Reference:
                case TokenType.Section:
                case TokenType.Table:
                case TokenType.Template:
                    break;
                case TokenType.Link:
                    Link link = (Link)token;
                    if (link.Namespace.Text.Equals (""))
                    {
                        count += link.Value.WordsCount ();
                    }
                    break;
                default:
                    count += token.Text.WordsCount ();
                    break;
            }
        }
        return count;
    }

    public Page ()
    {
        Query     = null;
        _tokens   = new Tokens();
        Templates = new List<Template>();
        Sections  = new List<Section>();
        Links     = new List<Link>();
	    MilHist   = new MilHist (this);
    }

    public Page (Bot bot, string title, int revid = -1) : this ()
    {
        Bot    = bot;
        Title  = title;
        RevId  = revid;
    }

}

using System;
using System.Collections.Generic;
using System.Linq;

public class Showcase: Page
{
    private const string showcasePageTitle = "Wikipedia:WikiProject Military history/Showcase/";

    private void updateCount (int d)
    {
    	Debug.Entered ();
        Comment comment = FindComment ("COUNT");
        int index = FindIndex (comment);
        Token countToken = GetToken (index + 1) as Token;
        Debug.WriteLine ("countToken = " + countToken.Text);
        int count = Convert.ToInt32 (countToken.Text);
        count += d; 
        countToken.Text = count.ToString ();
        Debug.Exited ("Count = " + count);
    } 
    
    new public void Add (string title)
    {
    	Debug.Entered ("title=" + title);    	
		var parameter = Templates.Last ().Find ("0");
		var tokens    = parameter.ValueTokens.Clone ();
		var input     = new IToken[] { new Token ("* "), new Link (title), new Token ("\n") };
    	
    	Link entry  = Links.Find (link => link.Namespace.Text.Equals ("") && String.Compare (link.Data, title) >= 0);
    	if (null == entry)
    	{
    		Debug.WriteLine ("null found - add to end of list");
    		entry = Links.FindLast (link => link.Namespace.Text.Equals (""));
			var index = tokens.Contents.FindIndex (x => x == entry);
    		tokens.Contents.InsertRange (index + 2, input);
    	}
    	else if (entry.Data.Equals (title))
    	{
    	    return;
    	}
    	else
    	{
			var index = tokens.Contents.FindIndex (x => x == entry);
    		tokens.Contents.InsertRange (index - 1, input);
    	}
    		
    	parameter.ValueTokens = tokens;
        updateCount (1);
        Debug.Exited ();        
    }
    
    public void Remove (string title)
    {
    	Debug.Entered ("title=" + title);    	
    	var entry = Links.Find (link => link.Data.Equals (title));
		var parameter = Templates.Last ().Find ("0");
		var tokens = parameter.ValueTokens.Clone ();
		var index = tokens.Contents.FindIndex (x => x == entry);
    	tokens.Contents.RemoveRange (index - 1, 3);
    	parameter.ValueTokens = tokens;
        updateCount (-1);        
        Debug.Exited ();        
    }
    
    public List<Page> Articles ()
    {
    	var articles = new List<Page>();
    	var links = Links.FindAll (x => x.Namespace.Text.Equals ("")); 
        foreach (var link in links)
        {
        	var page = new Page (Bot, link.Data);
        	articles.Add (page);
        }
		return articles;
    }  
           
    public Showcase (Bot bot, string title, int revId = -1): base (bot, showcasePageTitle + title, revId)
    {
        Load ();        
    }
       
}
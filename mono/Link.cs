using System;
using System.Collections.Generic;

public class Link : IToken
{
    public TokenType Type      { get; set; }
    public virtual string Text { get; set; }
    public Namespace Namespace { get; set; }
    public string Data         { get; set; }
    public string Redirect     { get; set; }
    
    public string Value
    {
        get => Redirect.Equals ("") ? Data : Redirect;
    }

    public override string ToString ()
    {
        return Text;
    }

	private Link specialLinks (Link link, Page page, Tokens contents)
	{
		if (ExtendedImage.IsExtendedImage (link))
		{
			return new ExtendedImage (page, contents);
		}
		return link;	
	}
	
    private void parse (Tokens tokens)
    {
        Tokens queue = tokens.Clone ();
        // Console.WriteLine ("Link = '" + queue.Text + "'");

        bool colon = false;
        if (queue.IsToken (TokenType.Colon))
        {
            colon = true;
            queue.Dequeue ();
        }

        string ns = "";
        if (queue.Any (2) && Namespace.IsNamespace (queue.Peek ().Text) && queue.Peek (1).Type == TokenType.Colon)
        {
            ns = queue.Dequeue ().Text;
            queue.Dequeue ();
        }
        Namespace = new Namespace (ns, colon);

        Redirect = "";
        Tokens data = new Tokens ();
        while (queue.Any ())
        {
            if (queue.IsToken (TokenType.Pipe))
            {
                queue.Dequeue ();
                Redirect = queue.Text;
                break;
            }
            data.Add (queue.Dequeue ());
        }
        Data = data.Text;
        // Console.WriteLine ("Data = '" + data + "' Redirect='" + Redirect + "'");
    }

    public Link (Page page, Tokens tokens)
    {
        Tokens contents = new Tokens ();
        while (tokens.Any ())
        {
            var token = tokens.Dequeue ();
            try
            {
                switch (token.Type)
                {
                    case TokenType.OpenComment:
                        contents.Add (new Comment (page, tokens));
                        break;
                    case TokenType.OpenNowiki:
                        contents.Add (new Nowiki (page, tokens));
                        break;
                    case TokenType.OpenBracket:
                        contents.Add (new Link (page, tokens));
                        break;
                    case TokenType.OpenTemplate:
                        contents.Add (new Template (page, tokens));
                        break;
                    case TokenType.CloseBracket:
                        parse (contents);
                        Text = "[[" + contents.Text + "]]";
                        Type = TokenType.Link;
    		            page.Links.Add (specialLinks (this, page, contents));
                        return;
                    default:
                        contents.Add (token);
                        break;
                }
            }
            catch (ParseException)
            {
                contents.Add (token);
            }
        }
        tokens.InsertRange (0, contents);
        throw new ParseException ("Unclosed link on " + page.Title + ": " + contents.Preview ());
    }
    
    public Link ()
    {    	
    }

    public Link (string data)
    {
        Text = "[[" + data + "]]";
        Type = TokenType.Link;
        Data = data;
        Redirect = "";
    }

    public Link (string ns, string data)
    {
        Text = "[[" + ns + ":" + data + "]]";
        Type = TokenType.Link;
        Namespace = new Namespace (ns);
        Data = data;
        Redirect = "";
    }

    public Link (string ns, string data, string redirect)
    {
        Text = "[[" + ns + ":" + data + "|" + redirect + "]]";
        Type = TokenType.Link;
        Namespace = new Namespace (ns);
        Data = data;
        Redirect = redirect;
    }
}

public class ExtendedImage : Link
{
	private Tokens contents;	
	
	public string Name      { get; set; }
	public string ImageType { get; set; }    
	public string Border    { get; set; }    
	public string Location  { get; set; }    
	public string Alignment { get; set; }    	
	public string Size      { get; set; }
	public string ImageLink { get; set; }
	public string Alt       { get; set; }    	
	public string ImagePage { get; set; } 
	public string Lang      { get; set; } 
	public string Caption   { get; set; } 
	
	public override string Text 
	{
		get
		{
			if (null == contents)
			{
				contents = new 	Tokens ();
				var pipe = "|";
				contents.Add (Namespace.Text, ":", Name);
				if (null != ImageType)
				{
					contents.Add (pipe, ImageType);					
				}
				if (null != Border)
				{
					contents.Add (pipe, Border);					
				}
				if (null != Location)
				{
					contents.Add (pipe, Location);					
				}
				if (null != Alignment)
				{
					contents.Add (pipe, Alignment);					
				}
				if (null != Size)
				{
					contents.Add (pipe, Size);					
				}
				if (null != ImageLink)
				{
					contents.Add (pipe, ImageLink);					
				}
				if (null != Alt)
				{
					contents.Add (pipe, Alt);					
				}
				if (null != ImagePage)
				{
					contents.Add (pipe, ImagePage);					
				}
				if (null != Lang)
				{
					contents.Add (pipe, Lang);					
				}
				if (null != Caption)
				{
					contents.Add (pipe, Caption);					
				}				
			}	
			return "[[" + contents.Text + "]]";
		}
		set
		{
			contents = new Tokens (value);
		}
	}
	
	static public bool IsExtendedImage (Link link)
	{
		return link.Namespace.Equals ("File");
	}
	
	private void parse (Tokens contents)
    {
    	var tokens = contents.Contents.FindAll (x => x.Type == TokenType.Text);
    	Name = tokens[0].Text;
    	tokens.RemoveAt (0);
    	foreach (var token in tokens)
    	{
    		var text = token.Text;
			if (text.Matches ("thumb|frame"))
			{
				ImageType = text;
			}	    		
			else if (text.Matches ("border"))
			{
				Border = text;
			}
			else if (text.Matches ("right|left|center|none"))
			{
				Location = text;
			}			
			else if (text.Matches ("baseline|middle|sub|super|text-top|text-bottom|top|bottom"))
			{
				Alignment = text;
			}			
			else if (text.Matches (@"upright|\d+\s*px"))
			{
				Size = text;
			}
			else if (text.Matches (@"upright|\d+\s*px"))
			{
				Size = text;
			}
			else if (text.Matches ("^link="))
			{
				ImageLink = text;	
			}
			else if (text.Matches ("^alt="))
			{
				Alt = text;
			}
			else if (text.Matches ("^page="))
			{
				ImagePage = text;
			}
			else if (text.Matches ("^lang="))
			{
				Lang = text;
			}
			else
			{
				Caption = text; 
			}
    	}
	}

	public ExtendedImage (string name): base ()
	{
		Name = name;
        Type = TokenType.ExtendedImage;
        Namespace = new Namespace ("File");
	}

	public ExtendedImage (Page page, Tokens contents): base () 
	{
        parse (contents);
        this.contents = contents;
        Type = TokenType.ExtendedImage;
        Namespace = new Namespace ("File");
	}
}

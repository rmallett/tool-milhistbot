using System;
using System.Linq;
using System.Collections.Generic;

public class Coordinators
{
    private Bot bot;
    private string project;
    private List<string> coordinators;

    public bool IsCoordinator (string name) 
    {
   		if (null == coordinators)
        {
        	coordinators = new List<string>();
            Page page = new Page (bot, "Template:@" + project);
            page.Load ();
            coordinators = page.FindAllNs ("User").ConvertAll (x => x.Data.Trim ());
        }
		return coordinators.Exists (x => x.Equals (name));
    }

    public Coordinators (Bot bot, string project)
    {
    	this.bot     = bot;
    	this.project = project;
    }	
}

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class Parameter : IToken
{
    public string Text { get { return ToString (); } }
    public TokenType Type { get; private set; }

    public string Name { get; private set; }
    public string Value { get; private set; }
    public bool IsDefault { get; set; }
    public bool IsSpaced { get; set; }

	private Tokens nameTokens;
	private Tokens valueTokens;

    public Tokens NameTokens
    {
        get => nameTokens;
        set 
        {
        	nameTokens = value;	
        	Name = removeComments (nameTokens);
        } 
    }

    public Tokens ValueTokens
    {
        get => valueTokens;
        set 
        {
        	valueTokens = value;	
        	Value = removeComments (valueTokens);
        } 
    }
    
    private string removeComments (Tokens input)
    {
        var contents = input.Contents.FindAll (ind=>ind.Type != TokenType.Comment); 
        var output = new Tokens (contents);
        return output.Text.Trim ();
    }       

    public override string ToString ()
    {
       return  IsDefault ? ValueTokens.Text : nameTokens.Text + (IsSpaced ? " = " : "=") + ValueTokens.Text;
    }
        
    public Parameter (Tokens nameTokens, Tokens valueTokens)
    {    	
        Type        = TokenType.Parameter;
        ValueTokens = valueTokens;
        NameTokens  = nameTokens;
        IsDefault   = false;
        IsSpaced    = false;
    }
    
    public Parameter (string name, string value) : this (new Tokens (name), new Tokens (value))
    {
    }     

    public Parameter (int id, Tokens valueTokens) : this (new Tokens (id.ToString ()), valueTokens)
    {
        IsDefault = true;
    }

    public Parameter (int id, string value) : this (id, new Tokens (value))
    {
    }     
}

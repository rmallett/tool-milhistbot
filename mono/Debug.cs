using System;
using System.Diagnostics;

public class Debug
{
    public static bool On { get; set; }
    public static Bot Bot { get; set; }

    public static string Caller (Int32 skipFrames = 2) 
    {
        var stackTrace = new StackTrace (new StackFrame (skipFrames));
        return stackTrace.GetFrame(0).GetMethod().Name;
    }
    
    public static void Entered (string message = "")
    {
        WriteLine (Caller () + " entered " + message);
    } 

    public static void Exited (string message = "")
    {
        WriteLine (Caller () + " exited " + message);
    } 
    
    public static void WriteLine (string message)
    {
        if (On)
        {
            if (null == Bot)
            {
                Console.Error.WriteLine (message);
            }
            else
            {
                Bot.Cred.Showtime (message);
            }
        }
    }
}

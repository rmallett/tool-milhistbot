using System;

public class Namespace: IToken
{
    static private string[] namespaces = new string []{  "Media", "Special", "Talk", "User", "User talk", "Project", "Project talk", "File", "File talk", 
                                            "Image", "Image talk", "MediaWiki", "MediaWiki talk", "Template", "Template talk", "Help", "Help talk", 
                                            "Category", "Category talk", 
                                            "WP", "Wikipedia", "Wikipedia Talk"};
                                            
    public string Text { get; }
    public TokenType Type { get; }

    static public bool IsNamespace (string ns)
    {
        return Array.Exists(namespaces, element => element == ns);
    } 
     
    public override string ToString ()
    {
        return Text;
    }
   
    public Namespace (string name)
    {
        Text = name;
        Type = TokenType.Namespace;
    }                                                           

    public Namespace (string name, bool colon)
    {
        Text = colon ? (":" + name) : name;
        Type = TokenType.Namespace;
    }                                                           
} 
using System;
using System.Collections.Generic;

public class Blockquote : IToken
{
    public Page Page { get; set; }
    public TokenType Type  { get; set; }
    public string Text { get; set; }
    public string Value { get; set; }

    public Blockquote (Page page, Tokens tokens)
    {
        Page = page;
        var contents = new Tokens();
        while (! tokens.Empty ())
        {
            var token = tokens.Dequeue ();
            switch (token.Type)
            {
                case TokenType.OpenComment:
                    contents.Add (new Comment (page, tokens));
                    break;
                case TokenType.OpenNowiki:
                    contents.Add (new Nowiki (page, tokens));
                    break;
                case TokenType.OpenBracket:
                    contents.Add (new Link (page, tokens));
                    break;
                case TokenType.OpenExternalLink:
                    contents.Add (new ExternalLink (page, tokens));
                   break;
                case TokenType.OpenTemplate:
					contents.Add (new Template (page, tokens));
                    break;
                case TokenType.OpenReference:
                    contents.Add (new Reference (page, token));
                    break;
                case TokenType.CloseBlockquote:
                    init (contents, token);
                    return;
                default:
                    contents.Add (token);
                    break;
            }
        }
        tokens.InsertRange (0, contents);
        throw new ParseException ("Unclosed <blockquote> on " + page.Title + ": " + contents.Preview ());
    }

    private void init (Tokens tokens, IToken token)
    {
        Value = tokens.Text;
        Text = "<blockquote>" + Value + token;
        Type = TokenType.Blockquote;
    }

    public override string ToString ()
    {
        return Text;
    }
}

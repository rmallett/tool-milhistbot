using System;
using System.Collections.Generic;

public class Reference : IToken
{
    public TokenType Type  { get; set; }
    public string Text { get; set; }

    public override string ToString ()
    {
        return Text;
    }

    public Reference (Page page, IToken open, Tokens tokens)
    {
        Tokens contents = new Tokens ();
        while (tokens.Any ())
        {
            var token = tokens.Dequeue ();
            try
            {
                switch (token.Type)
                {
                    case TokenType.OpenComment:
                        contents.Add (new Comment (page, tokens));
                        break;
                    case TokenType.OpenNowiki:
                        contents.Add (new Nowiki (page, tokens));
                        break;
                    case TokenType.OpenBracket:
                        contents.Add (new Link (page, tokens));
                        break;
                    case TokenType.OpenTemplate:
                        contents.Add (new Template (page, tokens));
                        break;
                    case TokenType.CloseReference:
                        Text = open + contents.Text + token;
                        Type = TokenType.Reference;
                        return;
                    default:
                        contents.Add (token);
                        break;
                }
            }
            catch (ParseException)
            {
                contents.Add (token);
            }
        }
        tokens.InsertRange (0, contents);
        throw new ParseException ("Unclosed reference on " + page.Title + ": " + contents.Preview ());
    }

    public Reference (Page page, IToken token)
    {
        Type = TokenType.Reference;
        Text = token.Text;
    }
}

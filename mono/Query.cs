public class Query
{
    public string User   { get; set; }
    public string Limit  { get; set; }
    public string Start  { get; set; }
    public string End    { get; set; }
    public string Title  { get; set; }
    public string Namespace { get; set; } 
    public bool Continue { get; set; } 
      
    public Query (string user = null, int limit = 10, string start = null, string end = null)
    {
        User  = user;
        Limit = limit.ToString();
        Start = start;
        End   = end;
        Title = user;
        Continue = false;
    }           
     
}

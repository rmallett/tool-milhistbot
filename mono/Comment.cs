using System;
using System.Collections.Generic;

public class Comment : IToken
{
    public TokenType Type  { get; set; }
    public string Text { get; set; }
    public string Value { get; set; }

    public Comment (Page page, Tokens tokens)
    {
        var contents = new Tokens();
        while (! tokens.Empty ())
        {
            var token = tokens.Dequeue ();
            switch (token.Type)
            {
                case TokenType.CloseComment:
                    init (contents);
                    return;
                default:
                    contents.Add (token);
                    break;
            }
        }
	// Unclosed comments do continue to the end of the page!
        init (contents);
    }

    private void init (Tokens tokens)
    {
        Value = tokens.Text;
        Text = "<!--" + Value + "-->";
        Type = TokenType.Comment;
    }

    public override string ToString ()
    {
        return Text;
    }
}

#!/usr/bin/perl -w
#
# MilHist::ArticleHistory.pm
#    30 June 2018 Created
#    25 July 2018 Added parser routine
#    11 December 2018 Added find_action
#     8 July 2019 Allowing for gaps in action values

package MilHist::ArticleHistory;

use English;
use strict;
use utf8;
use warnings;

use Data::Dumper;
use Date::Parse;
use MilHist::Parser;
use MilHist::Template;

my %fields;

sub get_revid ($$$) {
	my ($self, $date, $time) = @ARG;
	my $editor = $self->editor ();
	my $page   = $self->page();
	my $continue;
	my $history;
	do {
		($history, $continue) = $editor->fetch_history ($page, $continue) or
			$editor->error ("Unable to fetch history of '$page'");
		foreach my $history (@$history) {
			# warn "timestamp= $history->{timestamp} <=> ${date}T${time}Z\n";
			if ($history->{timestamp} le "${date}T${time}Z") {
				return $history->{revid};
			}
		}
	} while ($continue);
	$editor->error ("Unable to get revid of '$page')");
}

sub parse_display_date ($$) {
	my ($self, $display_date) = @ARG;
	my $editor = $self->editor ();
	my ($ss, $mm, $hh, $day, $month, $year, $zone) = strptime ($display_date) or
	  $editor->error ("Unable to parse date '$display_date'");
	my $dt = DateTime->new (
		year   => 1900 + $year,
		month  => 1 + $month,
		day    => $day,
		hour   => $hh // 0,
		minute => $mm // 0,
		second => $ss // 0,
	);

	my $date = $dt->strftime ("%Y-%m-%d");
	my $time = $dt->strftime ("%H:%M");
	return ($date, $time);
}

sub has_dup_ga ($$) {
	my ($self, $link) = @ARG;
	# print "link='$link'\n";
	my $template = $self->{'template'};
	my @actions = $template->find('action\\d+link$');
	foreach my $action (@actions) {
		# printf "value='%s'\n", $action->value;
		if ($action->value eq $link) {
			# print "found!\n";
			return 1;
		}
	}
	return 0;
}

sub add_ga_to_history ($) {
	my ($self)   = @ARG;
	my $parser   = $self->parser ();
	my $editor   = $self->editor ();
	my $cred     = $editor->cred ();
	my $template = $parser->find ('^GA$');
	if (defined $template) {
		$cred->showtime ("\tMerging GA into article history\n");
		my $display_date = $template->get ('date') // $template->get (1);
		my $pageno       = $template->get ('page') // 1;
		my $talk         = $self->talk ();
		my $link         = "$talk/GA$pageno";
		my $topic        = $template->get ('topic');
		my $revid = $template->get ('oldid');
		if (!defined $revid || $revid !~ /\d+/) {
			my ($date, $time) = $self->parse_display_date ($display_date);
			$revid = $self->get_revid ($date, $time);
		}
		$parser->delete ($template);
		unless ($self->has_dup_ga ($link)) {
			$self->add_action ('GAN', $display_date, $link, 'listed', $revid);
		}
		$self->add ('topic' => $topic);
	}
}

sub add_pr_to_history ($) {
	my ($self)   = @ARG;
	my $parser   = $self->parser ();
	my $editor   = $self->editor ();
	my $cred     = $editor->cred ();
	my $page     = $self->page ();
	my $template = $parser->find ('^(oldpeerreview|Old Peer Review)$');
	if (defined $template) {
		$cred->showtime ("\tFound old Peer Review\n");
		my $archive = $template->get ('archive');
		my $name    = $template->get ('reviewedname') // $page;
		my $link    = $template->get ('archivelink')  // ("Wikipedia:Peer_review/$name" . ($archive ? "/archive$archive" : ''));
		my $date    = $template->get ('date');
		my $revid   = $template->get ('ID');

		unless (defined $date && defined $revid ) {
			my ($history) = $editor->get_history ($link) or
			  $editor->error ("Unable to get history of '$link'");
			$date  = $history->{timestamp_date};
			my $time  = $history->{timestamp_time};
			$revid = $self->get_revid ($date, $time);
		};

		$parser->delete ($template);
		$self->add_action ('PR', $date, $link, 'reviewed ', $revid);
	}
}

sub add_afd_to_history ($) {
	my ($self)   = @ARG;
	my $parser   = $self->parser ();
	my $editor   = $self->editor ();
	my $cred     = $editor->cred ();
	my $page     = $self->page ();
	my $template = $parser->find ('^(old AfD|Old Afd multi)$');
	if (defined $template) {
		$cred->showtime ("\tFound old AfD\n");
		my $name = $template->get ('page') // $page;
		my $link = "Wikipedia:Articles for deletion/$name";
		my $date;
		my $time;
		my $display_date = $template->get ('date');
		if ($display_date) {
			($date, $time) = $self->parse_display_date ($display_date);
		} else {
			my ($history) = $editor->get_history ($link) or
			  $editor->error ("Unable to get history of '$link'");
			$date  = $history->{timestamp_date};
			$time  = $history->{timestamp_time};
			$display_date = "$time $date";
		}
		my $revid = $self->get_revid ($date, $time);
		my $result = $template->get('result');
		$result =~ s/\'//g;
		$parser->delete ($template);
		$self->add_action ('AFD', $display_date, $link, $result, $revid);
	}
}

sub add_dyk_to_history ($) {
	my ($self)   = @ARG;
	my $parser   = $self->parser ();
	my $editor   = $self->editor ();
	my $cred     = $editor->cred ();
	my $template = $parser->find ('^dyk talk|dyktalk$');
	if (defined $template) {
		$cred->showtime ("\tAdd DYK to ArticleHistory\n");
		my $dykdate  = $template->get (1);
		my $dykentry = $template->get ('entry') // $template->get (2);
		my $dyknom   = $template->get ('nompage');
		my $dykyear  = $template->get (2) // '';
		if ($dykyear =~ /^\d\d\d\d$/) {
			# Old format
			$dykdate = join ' ', $dykdate, $dykyear;
		}
		$parser->delete ($template);
		$self->add ('dykdate'  => $dykdate);
		$self->add ('dykentry' => $dykentry);
		$self->add ('dyknom'   => $dyknom) if $dyknom;
	}
}

sub add_itn_to_history ($) {
	my ($self)   = @ARG;
	my $parser   = $self->parser ();
	my $editor   = $self->editor ();
	my $cred     = $editor->cred ();
	my $template = $parser->find ('^itn talk|itntalk$');
	if (defined $template) {
		$cred->showtime ("\tAdd ITN to ArticleHistory\n");
		my $d1   = $template->get (1);
		my $d2   = $template->get (2);
		if ($d1 && $d2) {
			my $itndate = "$d1 $d2";
			$self->add ('itndate' => $itndate);
		}
		my $itndate  = $template->get ('date');
		if ($itndate) {
			$self->add ('itndate' => $itndate);
		}
		for (my $i = 1; ; ++$i) {
			my $itndate  = $template->get ("date${i}");
			if ($itndate) {
				$self->add ("itn${i}date" => $itndate);
				next;
			}
			last;
		}
		$parser->delete ($template);
	}
}

sub add_otd_to_history ($) {
	my ($self)   = @ARG;
	my $parser   = $self->parser ();
	my $editor   = $self->editor ();
	my $cred     = $editor->cred ();
	my $template = $parser->find ('^OTD talk|OTDtalk|On this day|OnThisDay$');
	if (defined $template) {
		$cred->showtime ("\tAdd OTD to ArticleHistory\n");
		for (my $i = 1; ; ++$i) {
			my $otddate  = $template->get ("date${i}");
			if ($otddate) {
				$self->add ("otd${i}date" => $otddate);
				my $oldid = $template->get ("oldid${i}");
				if (! defined $oldid) {
					my ($date, $time) = $self->parse_display_date ($otddate);
					$oldid = $self->get_revid ($date, $time);
				}
				$self->add ("otd${i}oldid" => $oldid);
				next;
			}
			last;
		}
		$parser->delete ($template);
	}
}

sub merge ($) {
	my ($self) = @ARG;
	$self->add_afd_to_history ();
	$self->add_ga_to_history ();
	$self->add_pr_to_history ();
	$self->add_dyk_to_history ();
	$self->add_itn_to_history ();
	$self->add_otd_to_history ();
}

sub get_action_id ($$) {
	my ($self, $id) = @ARG;
	my $template = $self->{'template'};
	my $action = {};
	$action->{'id'}     = $id;
	$action->{'action'} = $self->get("action${id}");
	$action->{'date'}   = $self->get("action${id}date");
	$action->{'link'}   = $self->get("action${id}link");
	$action->{'result'} = $self->get("action${id}result");
	$action->{'oldid'}  = $self->get("action${id}oldid");
	return $action;
}

sub add_action_id ($$$$$$$) {
	my ($self, $id, $action, $date, $link, $result, $oldid) = @ARG;

	$self->add("action${id}"       => $action);
	$self->add("action${id}date"   => $date);
	$self->add("action${id}link"   => $link);
	$self->add("action${id}result" => $result);
	$self->add("action${id}oldid"  => $oldid);
}

sub move_to_end ($$) {
	my ($self, $name) = @ARG;
	my $template = $self->{'template'};
	my $parameters = $template->parameters;
	my $i;
	for ($i = 0; $i < scalar @$parameters; ++$i) {
		if 	($parameters->[$i]->name =~ /$name/i) {
			# print "found $name\n";
			last;
		}
	}
	if ($i < scalar @$parameters) {
		my $p = splice @$parameters, $i, 1;
		push @$parameters, $p;
	}
}


sub sort ($) {
	my ($self) = @ARG;
	my $template = $self->{'template'};
	my $changed = 0;
	my @actions;

	for (my $id = 1;; ++$id) {
		my $parameter = $template->find("action$id");
		if (not defined $parameter) {
			last;
		}
		my $action = $self->get_action_id ($id);
		push @actions, $action;
	}

	@actions = sort { str2time ($a->{'date'}) <=> str2time ($b->{'date'}) } @actions;
	my $n = scalar @actions;

	for (my $id = 1; $id <= $n; ++$id) {
		my $action = $actions[$id-1];
		if ($id != $action->{'id'}) {
			$changed = 1;
			$self->add_action_id ($id, $action->{'action'}, $action->{'date'}, $action->{'link'}, $action->{'result'}, $action->{'oldid'});
		}
	}

	$self->move_to_end ('^\s*currentstatus$');
	$self->move_to_end ('^\s*topic$');

	return $changed;
}

sub add_action ($$$$$$) {
	my ($self, $action, $date, $link, $result, $oldid) = @ARG;

	my $template = $self->{'template'};
	for (my $id = 1;; ++$id) {
		my $parameter = $template->find("action$id");
		if (not defined $parameter) {
			$self->add_action_id ($id, $action, $date, $link, $result, $oldid);
			last;
		}
	}
}

sub find_action ($$$) {
	my ($self, $lookup_action, $lookup_param) = @ARG;

	my $value;
	my $template = $self->{'template'};
	my @actions = $template->find('action\\d+$');
	foreach my $action (reverse @actions) {
		if ($action->value eq $lookup_action) {
			my $name = $action->name;
			$value = $template->get("$name${lookup_param}\$");
			last;
		}
	}
	return $value;
}

sub add ($$$) {
	my ($self, $key, $value) = @ARG;
	my $template = $self->{'template'};
	$template->add($key => $value . "\n");
}

sub get ($$) {
	my ($self, $key) = @ARG;
	my $template = $self->{'template'};
	return $template->get($key);
}

sub text ($) {
	my ($self) = @ARG;
	return $self->{'parser'}->text();
}

sub new {
	my $that  = shift;
	my $class = ref($that) || $that;
	my $self  = {%fields, @ARG};
	bless $self, $class;

	if ($self->{'text'}) {
		my $text   = $self->{'text'};
		my $parser = new MilHist::Parser('text' => $text);
		$self->{'parser'} = $parser;
	}

	my $parser   = $self->{'parser'};
	my $template = $parser->find('ArticleHistory|Article History');
	if (!defined $template) {
		$template = new MilHist::Template('name' => "ArticleHistory\n");
		$parser->addTop($template, "\n");
	}

	$self->{'template'} = $template;
	return $self;
}

sub AUTOLOAD {
	my $self = shift;
	my $type = ref ($self) or
	  Carp::croak "$self is not an object";
	my $name = our $AUTOLOAD;
	$name =~ s/.*://;
	defined $self->{$name} or
	  Carp::croak "Can't access '$name' field in object of class $type";
	$self->{$name} = shift if (@ARG);
	return $self->{$name};
}

sub DESTROY {
	my $self = shift;
}

1;

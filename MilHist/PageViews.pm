#!/usr/bin/perl -w
#
# MilHist::Pageviews.pm - Wiki page views class
#
#    11 Dec 2018 Created

package MilHist::PageViews;

use English;
use strict;
use utf8;
use warnings;

use Carp;
use DateTime;
use Data::Dumper;
use HTTP::Headers;
use HTTP::Request;
use JSON;
use LWP::UserAgent;
use URI::Escape;
use URI::URL;

my %options;

sub views ($$) {
	my ($self, $article, $main_date) = @ARG;

	$article =~ s/ /_/g;
	$article = ucfirst $article;
	$article = uri_escape_utf8 ($article);
	my $date = $main_date->strftime ("%Y%m%d00");
	my $url = new URI::URL ("http://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/user/$article/daily/$date/$date");
#	print "url='$url'\n";

	my $ua  = new LWP::UserAgent;
	my $headers = new HTTP::Headers ('User-Agent' => 'MegaBrowser/1.0');
	my $request = new HTTP::Request ('GET' => $url, $headers);
	my $response = $ua->request ($request);
	$response->is_success && $response->content !~ /Page Not Found/ or
      die "page '$url' not found\n";
	
	my $json = JSON->new->utf8;
	my $content = $json->decode ($response->content);
#	print Dumper $content;
	my $page_views = $content->{items}->[0]->{views};
#	print "'$article' has $page_views page views\n";
	return $page_views;
}

sub new {
	my $that = shift;
	my $class = ref ($that) || $that;
	my $self = { %options, @ARG };
	bless $self, $class;
	return $self;
}

1;
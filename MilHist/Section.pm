#!/usr/bin/perl -w
#
# MilHist::Section.pm - Wiki section class
# Sections consist of a name, a level, and text
#

package MilHist::Section;

use Carp;
use English;
use strict;
use warnings;

use MilHist::Parser;

sub _is_section ($$$) {
    my ($self, $elements, $pos) = @ARG;
    return ( !ref $elements->[$pos]
          && $elements->[$pos] =~ /==+/
          && defined $elements->[ $pos + 2 ]
          && !ref $elements->[ $pos + 2 ]
          && $elements->[ $pos + 2 ] eq $elements->[$pos]);
}

# The quoted match is importatnt here, as it may contain parentheses etc
sub _find ($$) {
    my ($self, $elements) = @ARG;
    my ($start, $end) = (-1, -1);
    my $level = 0;
    my $pos;
    for ($pos = 0; $pos < scalar @$elements; ++$pos) {
        if ($self->_is_section ($elements, $pos) && $elements->[ $pos + 1 ] =~ /\Q$self->{'name'}\E/) {
            $start = $pos;
            $level = length $elements->[$pos];
            last;
        }
    }

    $start >= 0 or
      Carp::confess "unable to find section '$self->{'name'}'";

    $end = scalar @$elements;
    for ($pos = $start + 3; $pos < $end; ++$pos) {
        if ($self->_is_section ($elements, $pos) && length $elements->[$pos] <= $level) {
            $end = $pos;
        }
    }
    $end--;
    return ($start, $end, $level);
}

sub _text ($$$) {
    my ($self, $elements, $start, $end) = @ARG;
    my @text;
    for (my $pos = $start; $pos <= $end; ++$pos) {
        my $element = $elements->[$pos];
        if (ref $element) {
            push @text, $element->text;
        } else {
            push @text, $element;
        }
    }
    my $text = join '', @text;
    return $text;
}

sub elements ($) {
    my ($self)   = @ARG;
    my $elements = $self->parser->elements;
    my @elements = @{$elements}[ $self->{'start'} .. $self->{'end'} ];        
    return @elements;
}

sub add ($$) {
    my ($self, $section) = @ARG;
    if (ref $section eq 'MilHist::Section') {
        my $elements = $self->parser->elements;
        splice @$elements, $self->{'end'} + 1, 0, $section->elements;
        $self->{'end'} += $section->end - $section->start + 2;
    } else {
        $self->{'text'} .= $section;    
    }
    return $self;
}

sub DESTROY {
    my $self = shift;
}

sub AUTOLOAD {
    my $self = shift;
    my $type = ref ($self) or
      Carp::croak "$self is not an object";
    my $name = our $AUTOLOAD;
    $name =~ s/.*://;
    defined $self->{$name} or
      Carp::croak "Can't access '$name' field in object of class $type";
    $self->{$name} = shift if (@ARG);
    return $self->{$name};
}

sub new {
    my $that   = shift;
    my $class  = ref ($that) || $that;
    my %fields = ('level' => 2);
    my $self   = {%fields, @ARG};
    bless $self, $class;

    defined $self->{'name'} or
      Carp::croak 'name required';

    defined $self->{'text'}     or
      defined $self->{'parser'} or
      Carp::croak 'text or parser required';

    if ($self->{'text'}) {
        my $text = join '', '=' x $self->{level}, ' ', $self->{'name'}, ' ', '=' x $self->{level}, "\n", $self->{text};
        my $parser = new MilHist::Parser ('text' => $text);
        $self->{'text'}   = $text;
        $self->{'parser'} = $parser;
        my $elements = $parser->elements;
        $self->{'start'} = 1;
        $self->{'end'}   = scalar @$elements - 1;

    } elsif ($self->{'parser'}) {
        my $parser   = $self->{'parser'};
        my $elements = $parser->elements;
        ($self->{'start'}, $self->{'end'}, $self->{'level'}) = $self->_find ($elements);
        $self->{'text'} = $self->_text ($elements, $self->{'start'}, $self->{'end'});
    }

    return $self;
}

1;

#!/usr/bin/perl -w
#
# MilHist::Parameter.pm - Wiki parameter class
# Parameter class contains:
#   name    - of the parameter
#   tokens  - a list of tokens making up its value, which may be text or templates
#   default - whether the name was auto-generated
#
# 15 Feb 2019 DESTROY method required with AUTOLOAD

package MilHist::Template::Parameter;

use Carp;
use English;
use strict;
use warnings;

my %fields = ();

sub value ($) {
    my $self = shift;
    my @text;
    my $tokens = $self->{tokens};
    foreach my $token (@$tokens) {
        if (ref $token) {
            push @text, $token->text ();
        } else {
            push @text, $token;
        }  
    }
    shift @text unless $self->{default};  
    my $text = join '', @text;
    $text =~ s/<!--.+?-->//gs;
    $text =~ s/\s+$//;
    return $text;
}

sub text ($) {
    my $self = shift;
    my @text;
    my $tokens = $self->{tokens};
    foreach my $token (@$tokens) {
        if (ref $token) {
            push @text, $token->text ();
        } else {
            push @text, $token;
        }  
    }
    if ($self->{'default'}) {
    	if (defined $text[0] && $text [0] eq ' = ') {
        	shift @text;
      	}
    } else {
    	unshift @text, $self->{'name'};
    }  
    my $text = join '', @text;
    return $text;
}

sub DESTROY {
	  my $self = shift;
}

sub AUTOLOAD {
  	my $self = shift;
  	my $type = ref ($self) or
  		  Carp::croak "$self is not an object";
  	my $name = our $AUTOLOAD;
  	$name =~ s/.*://;
  	defined $self->{$name} or
  		  Carp::croak "Can't access '$name' field in object of class $type";
  	$self->{$name} = shift if (@ARG);
  	return $self->{$name};
}

sub new {
  	my $that = shift;
  	my $class = ref ($that) || $that;
  	my $self = { %fields, @ARG };
  	bless $self, $class;
  	return $self;
}

1;
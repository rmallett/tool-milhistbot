#!/usr/bin/perl -w

use English;
use strict;
use warnings;

use Archive::Tar;
use DateTime;

my $home = '/data/project/milhistbot';
my $logs = "$home/logs";
my $archive = "$home/archive";

my @logs = glob ("$home/*.err $home/*.out $logs/*.log");
my $timestamp = DateTime->now (time_zone => 'UTC')->strftime ('%Y%m%d');

my $tar = Archive::Tar->new ();
$tar->add_files (@logs);
$tar->write ("$archive/archive-$timestamp.tgz", COMPRESS_GZIP);

unlink @logs;

exit 0;